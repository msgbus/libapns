//
//  YBWorkConsle.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/31.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <amqp_tcp_socket.h>
#include <amqp.h>
#include <amqp_framing.h>
#include <assert.h>
#include <sys/file.h>

#include "utils.h"
#include "YBWorkConsle.h"
#include "YBAPNs.h"
#include "YBAPNsDebug.h"
#include "YBCouchBase.h"
#include "cJSON.h"
#include "YBAPNsDebug.h"
#include "YBCertDownload.h"
#include "YBMqPayloadParse.h"

typedef enum YBAPNsPushType {
    YB_PUSH_TYPE_DEV = 0, // developement
    YB_PUSH_TYPE_PRD = 1 // product
}YBAPNsPushType;


typedef enum YBErr {
    YB_OK,
    YB_FAILURE,
}YBErr;


// couchbase -> data -> push_INFO
struct push_INFO {
    char *deviceToken;
    char *appKey;
    char *bundle_id;
    YBAPNsPushType type;
};


#pragma mark - tools
// 针对 push_INFO 和 mqPayload 的 清理
static void
push_cleaning_up(struct push_INFO *pi,struct mqPayload *mp) {
    if (pi) {
        if (pi->deviceToken) {free(pi->deviceToken);}
        if (pi->appKey) {free(pi->appKey);}
        if (pi->bundle_id) {free(pi->bundle_id);}
    }
    if (mp) {
        if (mp->alert) {free(mp->alert);}
        if (mp->sound) {free(mp->sound);}
        if (mp->uid) {free(mp->uid);}
        if (mp->bodyStr) {free(mp->bodyStr);}
    }
}
// void* 的拷贝函数
static void * payload_copy(const void *payload,size_t len) {
    void *copy = (void *)calloc(1, len);
    memcpy(copy, payload, len);
    return copy;
}

static uint32_t find_available_multi_curl(struct YBAPNsmulti_curl_set *multi_set) {
    for (int i = 0; i < multi_set->len; i++) {
        if (multi_set->multi_curl_set[i].used == 0) {
            return i;
        }
    }
    return -1;
}

#pragma mark - get
// 获取 推送相关信息
static YBErr
get_push_INFO(struct push_INFO *pi,const struct mqPayload mp,struct YB_couchbase_t *cb) {
    char *uid;
    
    uid = strdup(mp.uid);
    char * value = YB_couchbase_get_value(cb, uid);
    cJSON *json = cJSON_Parse(value);
    cJSON *token = cJSON_GetObjectItem(json, "t");
    cJSON *appKey = cJSON_GetObjectItem(json, "a");
    cJSON *type = cJSON_GetObjectItem(json, "e");
    cJSON *bundle_Id = cJSON_GetObjectItem(json, "b");
    if (!token || !appKey||
        !type || !bundle_Id) {
        YB_APNs_DEBUG("get json value failed:token:(%p),appkey:(%p),type:(%p),bundle_id:(%p)",
                      token,appKey,type,bundle_Id);
        return YB_FAILURE;
    }
    pi->deviceToken = strdup(token->valuestring);
    pi->appKey = strdup(appKey->valuestring);
    char *typeString = type->valuestring;
    pi->type = strcmp(typeString, "development") == 0 ? YB_PUSH_TYPE_DEV : YB_PUSH_TYPE_PRD;
    pi->bundle_id = strdup(bundle_Id->valuestring);
    YB_APNs_LOG("====== get push_INFO ======\ntoken:%s\nappkey:%s\ntype:%d\nbundle_id:%s\n===========================",pi->deviceToken,pi->appKey,pi->type,pi->bundle_id);
    free(value);
    cJSON_Delete(json);
    free(uid);
    
    return YB_OK;
}

// 下载证书
static char *
download_certificate(struct push_INFO *pi,struct YB_cert_download_connection *certConn) {
    char * appkey, *bundle_id, *certUrl;
    char * pushTypeStr, *filePath,*timestampPath;
    YBAPNsPushType pushType;
    
    appkey = pi->appKey;
    bundle_id = pi->bundle_id;
    pushType = pi->type;
    pushTypeStr = pushType == YB_PUSH_TYPE_DEV ? "dev" : "pro";
    filePath = cert_download_storePath(appkey, bundle_id, pushTypeStr);
    timestampPath = cert_download_storeTimestamp(appkey,bundle_id,pushTypeStr);
    if (cert_exist(filePath)) {
        if(!cert_download_verify_timeout(timestampPath,con_param->cert_timeout)) {
            YB_APNs_LOG("cert do not time out,reuse cert",NULL);
            free(timestampPath);
            return filePath;
        }
        
    }
    free(timestampPath);
    certUrl = cert_download_url(appkey, bundle_id, pushTypeStr);
    if (!YB_cert_download(certConn, certUrl, filePath,timestampPath)) {
        return NULL;
    }
    free(certUrl);
    return filePath;
}

static void
connection_prepare(YBAPNsConnection * apnsConn,struct push_INFO *pi,struct mqPayload *mp,const char * cerPath) {
    YB_APNs_SetConfig(pi->type == YB_PUSH_TYPE_PRD ?
                      YB_APNS_PUSH_PRD_ADDR : YB_APNS_PUSH_DEV_ADDR,
                      YBAPNsConfigServer);
    YBAPNsRequestObject *request = YB_connection_getRequest(apnsConn);
    YBAPNsRequestHeader *header = YB_request_getHeader(request);
    YBAPNsRequestBody *body = YB_request_getBody(request);
    YB_request_setCer(request, "PEM", cerPath, NULL);
    YB_request_setAttribute(header, pi->deviceToken, 0, kYBAPNsRequestAttributeDeviceToken);
    YB_request_setAttribute(header, pi->bundle_id, 0, kYBAPNsRequestAttributeApnsTopic);
    if (mp->isBodyStr) {
        YB_request_setPayload(body, mp->bodyStr, 0, kYBAPNsRequestPayloadBodyStr);
    }else {
        YB_request_setPayload(body, mp->alert, 0, kYBAPNsRequestPayloadAlert);
        YB_request_setPayload(body, mp->sound, 0, kYBAPNsRequestPayloadSound);
        YB_request_setPayload(body, NULL, mp->badge, kYBAPNsRequestPayloadBadge);
        YB_request_setPayload(body, NULL, 0, kYBAPNsRequestPayloadBodyStr);
    }
}

#pragma mark - push module
/*********************** push module ******************************/
/******************************************************************/

static YBErr
push(YBAPNsConnection * apnsConn,struct push_INFO *pi,struct mqPayload *mp,const char * cerPath) {
    connection_prepare(apnsConn, pi, mp, cerPath);
    YBAPNsResponseObject *response = YB_connection_perform(apnsConn);
    if (!response) { return YB_FAILURE; }
    printf("==> get a response\n");
    printf("statusCode : %d\n",response->statusCode);
    printf("apnsId : %s\n",response->apnsId);
    printf("reason : %s\n",YBAPNsResReasonStr[response->reason]);
    printf("timestamp : %s\n",response->timestamp);
    YB_response_clean(response);
    return YB_OK;
}

static YBErr
multi_push(struct YBAPNsmulti_curl_set *ms,struct YB_couchbase_t *cb,
           struct YB_cert_download_connection *certConn) {
    int flag;
    for (int i = 0; i < ms->len; i++) {
        YBErr err;
        struct mqPayload mp;
        struct push_INFO pi = {0};
        struct YBAPNsMulti_curl *mc;
        
        mc = &(ms->multi_curl_set[i]);
        if (mc->used == 0) {
            continue;
        }
        mp = payload_parse(mc->data);
        err = get_push_INFO(&pi,mp,cb);
        if (YB_OK != err) {
            YB_APNs_DEBUG("couldn't get push info,push object:%d(%p)",i,ms);
            mc->used = 0;
            continue;
        }
        char * cerPath = download_certificate(&pi,certConn);
        if (!cerPath) {
            YB_APNs_DEBUG("couldn't get a ssl certificate,push object:%d(%p)",i,ms);
            mc->used = 0;
            continue;
        }
        connection_prepare(mc->conn, &pi, &mp, cerPath);
        if (!YB_connection_curl_prepare(mc->conn)) {
            YB_APNs_DEBUG("couldn't prepare curl,%d(%p)",i,ms);
            mc->used = 0;
            continue;
        }
        push_cleaning_up(&pi,&mp);
    
    }
    flag = YB_connection_multi_perform(ms);
    if (-1 == flag) {
        return YB_FAILURE;
    }else if (0 == flag) {
        struct YBAPNsMulti_curl *mc;
        
        for (int i = 0; i < ms->len; i++) {
            mc = &(ms->multi_curl_set[i]);
            void *data = mc->data;
            if (1 == mc->used) {
                if (!YB_connection_multi_curl_init(mc)) {
                    mc->used = 0;mc->added = 0;
                    continue;
                }
                mc->set = ms; mc->used = 1;mc->added = 0;mc->data = data;
            }
        }
        multi_push(ms, cb, certConn);
    }
    return YB_OK;
}


#pragma mark - handle
/*********************** handle ***********************************/
/******************************************************************/

static void
multi_unsend_task_handle(struct YBAPNsmulti_curl_set *set,struct YB_couchbase_t *couchbase,struct YB_cert_download_connection *certConn) {
    for (int i = 0; i < con_param->multi_Width; i++) {
        if (set->multi_curl_set[i].used == 1) {
            YB_APNs_LOG("===> start push notification",NULL);
            YBErr err;
            err = multi_push(set, couchbase, certConn);
            if (YB_FAILURE == err) {
                YB_APNs_LOG("multi task failed...",NULL);
            }
            YB_APNs_LOG("<=== push notification success",NULL);
            break;
        }
    }
}

static YBErr
multi_task_handle(const void *payload,
                  size_t len,
                  struct YBAPNsmulti_curl_set *ms,
                  struct YB_couchbase_t *cb,
                  struct YB_cert_download_connection *certConn) {
    uint32_t am; void *oldPayload;YBErr err;
    YB_APNs_LOG("=====> start a multi task handle",NULL);
    YB_APNs_LOG("finding a unused curl...",NULL);
    am = find_available_multi_curl(ms);
    YB_APNs_LOG("find:%d",am);
    if (am == -1) {
        return YB_FAILURE;
    }
    oldPayload = ms->multi_curl_set[am].data;
    if (oldPayload) {
        free(oldPayload);
    }
    YB_APNs_LOG("store payload...",NULL);
    ms->multi_curl_set[am].data = payload_copy(payload, len);
    ms->multi_curl_set[am].used = 1;
    if (am == ms->len - 1) {
        YB_APNs_LOG("===> set is full,start push notification",NULL);
        err = multi_push(ms,cb,certConn);
        if (YB_FAILURE == err) {
            YB_APNs_DEBUG("multi task failed...",NULL);
            return YB_FAILURE;
        }
        YB_APNs_LOG("<=== push notification done",NULL);
    }
    YB_APNs_LOG("<===== multi task handle done",NULL);
    return YB_OK;
}

static void
task_handle(const void *payload,
            YBAPNsConnection *apnsConn,
            struct YB_couchbase_t *cb,
            struct YB_cert_download_connection *certConn) {
    YBErr err;
    struct mqPayload mp;
    struct push_INFO pi = {0};
    
    mp = payload_parse(payload);
    err = get_push_INFO(&pi,mp,cb);
    if (YB_OK != err) {
        YB_APNs_DEBUG("couldn't get push info,push failed",NULL);
        return;
    }
    char * cerPath = download_certificate(&pi,certConn);
    if (!cerPath) {
        YB_APNs_DEBUG("couldn't get a ssl certificate,push failed",NULL);
        return;
    }
    err = push(apnsConn,&pi,&mp,cerPath);
    if (YB_OK != err) {
        YB_APNs_DEBUG("push failed",NULL);
    }
    free(cerPath);
    push_cleaning_up(&pi, &mp);
    
}

#pragma mark - work function
/*********************** work function *****************************/
/******************************************************************/

static amqp_connection_state_t connect_to_amqp() {
    int status;
    amqp_socket_t *socket = NULL;
    amqp_connection_state_t conn;
    amqp_bytes_t queuename;
    
    YB_APNs_LOG("create amqp conection...",NULL);
    conn = amqp_new_connection();
    YB_APNs_LOG("create TCP socket...",NULL);
    socket = amqp_tcp_socket_new(conn);
    if (!socket) {
        die("creating TCP socket failed");
    }
    YB_APNs_LOG("open TCP socket...",NULL);
    status = amqp_socket_open(socket, con_param->mq_host, con_param->mq_port);
    if (status) {
        die("open TCP socket failed");
    }
    YB_APNs_LOG("login to amqp server...",NULL);
    amqp_rpc_reply_t logErr =amqp_login(conn, "/", 0, 131072, 0, AMQP_SASL_METHOD_PLAIN, "guest", "guest");
    die_on_amqp_error(logErr,"login failed");
    YB_APNs_LOG("opening channel...",NULL);
    amqp_channel_open(conn, 1);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Opening channel failed");
    
    //create a queue...
    {
        amqp_queue_declare_ok_t *r;
        YB_APNs_LOG("declaring queue...",NULL);
        r = amqp_queue_declare(conn, 1, amqp_cstring_bytes(con_param->mq_queue), 0, 0, 0, 0,
                               amqp_empty_table);
        die_on_amqp_error(amqp_get_rpc_reply(conn), "Declaring queue failed");
        queuename = amqp_bytes_malloc_dup(r->queue);
        if (queuename.bytes == NULL) {
            die("Out of memory while copying queue name");
        }
    }
    
    // bind a queue to a work exchange if needed
    YB_APNs_LOG("bing queue to exchange...",NULL);
    amqp_queue_bind(conn, 1, queuename, amqp_cstring_bytes(con_param->mq_exchange), amqp_cstring_bytes(con_param->mq_routingKey),amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "Bind queue failed");
    
    amqp_basic_consume(conn, 1, queuename, amqp_empty_bytes, 0, 1, 0, amqp_empty_table);
    die_on_amqp_error(amqp_get_rpc_reply(conn), "consume failed");
    YB_APNs_LOG("========> start event loop...",NULL);
    
    return conn;
}

/*
 * amqp专用 event loop
 */
static void runLoop(amqp_connection_state_t conn,YBAPNsConnection *apnsConn,struct YBAPNsmulti_curl_set *set,struct YB_couchbase_t *couchbaseObj,struct YB_cert_download_connection *certConn) {
    amqp_frame_t frame;
    struct timeval timeout;
    int received = 0; // msg received count
    
    // event loop
    YB_time_start("t1");
    YB_count_start("c1");
    for (;;) {
        amqp_rpc_reply_t ret;
        amqp_envelope_t envelope;
        
        amqp_maybe_release_buffers(conn);
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        YB_APNs_LOG("=====> waiting for message...",NULL);
        ret = amqp_consume_message(conn, &envelope, con_param->isMulti ? &timeout : NULL, 0);
        if (AMQP_RESPONSE_NORMAL != ret.reply_type) {
            if (AMQP_RESPONSE_LIBRARY_EXCEPTION ==ret.reply_type &&
                AMQP_STATUS_TIMEOUT == ret.library_error) {
                YB_time("t1");
                // timeout
                YB_APNs_LOG("===> timeout,check message set",NULL);
                multi_unsend_task_handle(set, couchbaseObj, certConn);
                YB_APNs_LOG("<==== No message to push,handle done",NULL);
                
            }
            else if (AMQP_RESPONSE_LIBRARY_EXCEPTION == ret.reply_type &&
                     AMQP_STATUS_CONNECTION_CLOSED == ret.library_error) {
                YB_APNs_LOG("amqp connection has been closed, reconnect",NULL);
                conn = connect_to_amqp();
            }
            // unexpected error
            else if (AMQP_RESPONSE_LIBRARY_EXCEPTION == ret.reply_type &&
                AMQP_STATUS_UNEXPECTED_STATE == ret.library_error) {
                if (AMQP_STATUS_OK != amqp_simple_wait_frame(conn, &frame)) {
                    die("die on unexpected state reply_type:%d,library_error:%d",ret.reply_type,ret.library_error);
                }
                
                if (AMQP_FRAME_METHOD == frame.frame_type) {
                    switch (frame.payload.method.id) {
                        case AMQP_BASIC_ACK_METHOD:
                            /* if we've turned publisher confirms on, and we've published a message
                             * here is a message being confirmed
                             */
                            
                            break;
                        case AMQP_BASIC_RETURN_METHOD:
                            /* if a published message couldn't be routed and the mandatory flag was set
                             * this is what would be returned. The message then needs to be read.
                             */
                        {
                            amqp_message_t message;
                            ret = amqp_read_message(conn, frame.channel, &message, 0);
                            if (AMQP_RESPONSE_NORMAL != ret.reply_type) {
                                return;
                            }
                            
                            amqp_destroy_message(&message);
                        }
                            
                            break;
                            
                        case AMQP_CHANNEL_CLOSE_METHOD:
                            /* a channel.close method happens when a channel exception occurs, this
                             * can happen by publishing to an exchange that doesn't exist for example
                             *
                             * In this case you would need to open another channel redeclare any queues
                             * that were declared auto-delete, and restart any consumers that were attached
                             * to the previous channel
                             */
                            return;
                            
                        case AMQP_CONNECTION_CLOSE_METHOD:
                            /* a connection.close method happens when a connection exception occurs,
                             * this can happen by trying to use a channel that isn't open for example.
                             *
                             * In this case the whole connection must be restarted.
                             */
                            return;
                            
                        default:
                            fprintf(stderr ,"An unexpected method was received %u\n", frame.payload.method.id);
                            return;
                    }
                }
            }
            
        } else {
            YB_APNs_LOG("----->get a amqp message...",NULL);
            // msg handle
            if (con_param->isMulti) {
                YB_APNs_LOG("--> multi mode...",NULL);
                multi_task_handle(envelope.message.body.bytes,
                                  envelope.message.body.len,
                                  set,
                                  couchbaseObj,
                                  certConn);
            }else {
                YB_APNs_LOG("-->single task begin...",NULL);
                task_handle(envelope.message.body.bytes,apnsConn,couchbaseObj,certConn);
            }
            amqp_destroy_envelope(&envelope);
            received++;
            YB_APNs_LOG("<-----message handle complete message received count:%d",received);
        }
    }
}

/*
 * 核心工作函数
 */
void
work(void){
    // amqp connection init
    amqp_connection_state_t conn;
    conn = connect_to_amqp();
    
    // APNs connection init
    YB_connection_global_init();
    YBAPNsConnection *apnsConn = NULL;
    struct YBAPNsmulti_curl_set set;
    if (con_param->isMulti) {
        YB_APNs_LOG("multi mode, create curl multi set...",NULL);
        if (!YB_connection_multi_set_init(&set, con_param->multi_Width,con_param->max_conn)) {
            die("multi curl init failed,exit");
        }
    }else {
        YB_APNs_LOG("single mode,create single curl connection...",NULL);
        apnsConn = YB_connection_get_default_conn();
        if (NULL == apnsConn) {
            die("single curl init failed,exit");
        }
    }
    
    // couchbase connection init
    YB_APNs_LOG("-->couchbase connecting...",NULL);
    struct YB_couchbase_t couchbaseObj;
    couchbaseObj.cb = con_param->couchbase;
    couchbaseObj.psw = NULL;
    if (!YB_couchbase_create(&couchbaseObj)) {
        die("couchbase connection create failed,exit");
    }
    
    // cert download connection init
    YB_APNs_LOG("-->cert Server connecting...",NULL);
    struct YB_cert_download_connection certConn;
    if (!YB_cert_download_connection_create(&certConn)) {
        die("cert download connection create failed,exit");
    }
    YB_APNs_LOG("<--connection all setup,begin runloop...",NULL);
    
    runLoop(conn,apnsConn,&set,&couchbaseObj,&certConn);
    
    if(apnsConn){YB_connection_clean(apnsConn);}
    YB_connection_multi_curl_set_cleaning_up(&set);
    YB_couchbase_cleaning_up(&couchbaseObj);
    YB_cert_download_cleanning_up(&certConn);
    
    die_on_amqp_error(amqp_channel_close(conn, 1, AMQP_REPLY_SUCCESS), "Close channel failed");
    die_on_amqp_error(amqp_connection_close(conn, AMQP_REPLY_SUCCESS), "Close connection failed");
    die_on_error(amqp_destroy_connection(conn), "Ending connection failed");
    
}