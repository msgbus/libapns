//
//  YBCouchBase.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/1.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//
#include "YBCouchBase.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h> /* strlen */
#include <inttypes.h>
#include "YBAPNsDebug.h"

static void
bug_log(lcb_t instance,const char *msg,lcb_error_t err) {
    YB_APNs_DEBUG("%s. Received code 0x%X (%s)",
                  msg,err,lcb_strerror(instance, err));
}
// 取值操作的callback
static void command_get_callback(lcb_t instance,int cbType, const lcb_RESPBASE *rp) {
    YB_APNs_LOG("==== coucbase %s ====\n",lcb_strcbtype(cbType));
    if (LCB_SUCCESS == rp->rc) {
        YB_APNs_LOG( "KEY:%.*s\n",(int)rp->nkey,rp->key);
        YB_APNs_LOG("CAS:0x%llu\n",rp->cas);
        // 有值的时候
        if (LCB_CALLBACK_GET == cbType) {
            const lcb_RESPGET *rg = (const lcb_RESPGET *)rp;
            YB_APNs_LOG("VALUE:%.*s\n",(int)rg->nvalue,rg->value);
            YB_APNs_LOG("flag:0x%X\n",rg->itmflags);
            char **userdata = (char **)lcb_get_cookie(instance);
            // 用户数据，会存入获取到的json
            *userdata = strndup((char *)rg->value, (int)rg->nvalue);
        }
        
    }else {
        // 取不到值得时候
        YB_APNs_DEBUG("%s. Received code 0x%X (%s)",
        lcb_strcbtype(rp->rc), rp->rc,lcb_strerror(instance, rp->rc));
        /* get failed handle */
        
        //die(instance, lcb_strcbtype(rp->rc), rp->rc);
    }
}

int YB_couchbase_create(struct YB_couchbase_t *couchbase_t) {
    lcb_error_t err;
    struct lcb_create_st *create_opt = &(couchbase_t->create_opt);
    lcb_t *instance = &(couchbase_t->instance);
    memset(create_opt, 0, sizeof(*create_opt));
    create_opt->version = 3;
    create_opt->v.v3.connstr = couchbase_t->cb;
    create_opt->v.v3.passwd = couchbase_t->psw;
    
    // instance create
    err = lcb_create(instance, create_opt);
    if (LCB_SUCCESS != err) {
        bug_log(*instance, "lcb_t create failed", err);
        return 0;
    }
    
    // connecting
    err = lcb_connect(*instance);
    if (LCB_SUCCESS != err) {
        bug_log(*instance, "couldn't schedule connection", err);
        return 0;
    }
    // 阻塞 等待 lcb_connect
    YB_APNs_LOG("==> waiting for couchbase connection\n",NULL);
    lcb_wait(*instance);
    YB_APNs_LOG("<== couchbase connection success\n",NULL);
    
    // bootstap
    err = lcb_get_bootstrap_status(*instance);
    if (LCB_SUCCESS != err) {
        bug_log(*instance, "couldn't bootstrap cluster", err);
        return 0;
    }
    // 阻塞 等待 lcb_get_bootstrap_status
    YB_APNs_LOG("==> waiting for bootstrap\n",NULL);
    lcb_wait(*instance);
    YB_APNs_LOG("<== bootstrap success\n",NULL);
    // 设置 callback
    lcb_install_callback3(*instance, LCB_CALLBACK_GET, command_get_callback);
    
    return 1;
}


char * YB_couchbase_get_value(struct YB_couchbase_t *cb,const char *key) {
    lcb_error_t err;
    lcb_t *instance = &(cb->instance);
    lcb_CMDGET get = {0};
    char *userData; /* user data for callback */
    
    // get operation
    LCB_CMD_SET_KEY(&get, key, strlen(key));
    lcb_set_cookie(*instance, &userData);
    err = lcb_get3(*instance, NULL, &get);
    if (LCB_SUCCESS != err) {
        bug_log(*instance, "couldn't schedule get operation", err);
        return NULL;
    }
    YB_APNs_LOG("===> waiting for couchbase get operation",NULL);
    lcb_wait(*instance);
    YB_APNs_LOG("<=== couchbase get done",NULL);
    lcb_set_cookie(*instance, NULL);
    
    return userData;
}

void YB_couchbase_cleaning_up(struct YB_couchbase_t *couchbase) {
    if (couchbase) {lcb_destroy(couchbase->instance);}
}