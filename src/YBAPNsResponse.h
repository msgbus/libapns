//
//  YBAPNsResponse.h
//  YBApns2
//
//  Created by 云巴pro on 16/8/15.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBAPNsResponse_h
#define YBAPNsResponse_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>


typedef enum YBAPNsResCode {
    kYBAPNsResCodeSuccess = 200, // request succeed
    kYBAPNsResCodeBadRequest = 400,
    kYBAPNsResCodeCerErr = 403, // there was a error with the certificate
    kYBAPNsResCodeBadMethod = 405, // method value should be "POST"
    kYBAPNsResCodeTokenNoActive = 410,
    kYBAPNsResCodePayloadTooLarge = 413,
    kYBAPNsResCodeTooManyRequest = 429, // server received too many request for the same device token
    kYBAPNsResCodeInterServErr = 500, // Internal server error
    kYBAPNsResCodeServUnavailable = 503, // the server is shutting down and unavailable
    kYBAPNsResCodeNone = -1
}YBAPNsResCode; /* Response header statusCode */

typedef enum YBAPNsResReason {
    kYBResReasonPlayloadEmpty = 0,
    kYBResReasonPlayloadTooLarge = 1,
    kYBResReasonBadTopic = 2,
    kYBResReasonTopicDisallowed = 3,
    kYBResReasonBadMsgId = 4,
    kYBResReasonBadExpirationDate = 5,
    kYBResReasonBadPriority = 6,
    kYBResReasonMissingDevToken = 7,
    kYBResReasonBadDevToken = 8,
    kYBResReasonDevTokenNotForTopic = 9,
    kYBResReasonUnreg = 10,
    kYBResReasonDuplicateHeader = 11,
    kYBResReasonBadCerEnv = 12,
    kYBResReasonBadCer = 13,
    kYBResReasonForbidden = 14,
    kYBResReasonBadPath = 15,
    kYBResReasonMethodNotAllow = 16,
    kYBResReasonTooManyReq = 17,
    kYBResReasonIdleTimeout = 18,
    kYBResReasonServShutdown = 19,
    kYBResReasonInterServErr = 20,
    kYBResReasonServiceUnavailable = 21,
    kYBResReasonMissingTopic = 22,
    kYBResReasonNone = 23
}YBAPNsResReason; /* reason about statusCode in Response body  */

extern const char * const YBAPNsResReasonStr[];/* use YBAPNsResReasonStr[YBAPNsResReasonXXX] to search string for enum YBAPNsResReason*/

typedef struct YBAPNsResponseObject {
    char * apnsId; // if request omit this header, server create one and return
    YBAPNsResCode statusCode;
    YBAPNsResReason reason; // the reason for the failure
    char * timestamp; // if the value int the status header is 410,the value of this key is the last time at which APNs confrimed that the device token was no longer valid for the topic. stop pushing notification until the device registers a token with a later timestamp with your provider
}YBAPNsResponseObject;

/**
 * clean func for func for YBAPNsResponseObject every object must be clean when done
 */
extern void YB_response_clean(YBAPNsResponseObject * object);


extern YBAPNsResReason YB_response_search(const char *str);



///**
// *  getMethod
// */
//
//// header and body
//extern char * YB_response_getApnsId(YBAPNsResponseObject * const header);
//extern YBAPNsResCode YB_response_getStatusCode(YBAPNsResponseHeader * const header);
//extern YBAPNsResReason YB_response_getReason(YBAPNsResponseBody * const body); // return null if payload is empty
//extern char * YB_response_getTimestamp(YBAPNsResponseBody * const body); // return null if payload is empty







#ifdef __cplusplus
}
#endif

#endif /* YBAPNsResponse_h */
