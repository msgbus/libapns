//
//  YBAPNsResponse.c
//  YBApns2
//
//  Created by 云巴pro on 16/8/15.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include "YBAPNsResponse.h"
#include "YBAPNsDebug.h"
#include <string.h>
#include <stdlib.h>

const char * const YBAPNsResReasonStr[] = {
    "PayloadEmpty",
    "PayloadTooLarge",
    "BadTopic",
    "TopicDisallowed",
    "BadMessageId",
    "BadExpirationDate",
    "BadPriority",
    "MissingDeviceToken",
    "BadDeviceToken",
    "DeviceTokenNotForTopic",
    "Unregistered",
    "DuplicateHeaders",
    "BadCerificateEnvironment",
    "BadCertificate",
    "Forbidden",
    "BadPath",
    "MethodNotAllowed",
    "TooManyRequests",
    "IdleTimeout",
    "Shutdown",
    "InternalServerError",
    "ServiceUnavailable",
    "MissingTopic",
    "kYBResReasonNone",
    
};

void YB_response_clean(YBAPNsResponseObject * object) {
    YB_APNs_LOG("==>start clean response:%p",object);
    YB_APNs_NULL_HANDLE(object, {return;}, "response object is (NULL),clean failed",NULL);
    if (object->apnsId) {free(object->apnsId);}
    if (object->timestamp) {free(object->timestamp);}
    free(object);
    YB_APNs_LOG("<==clean response success\n",NULL);
}

YBAPNsResReason YB_response_search(const char *str) {
    for (int i = 0; ; i++) {
        const char *tmp = YBAPNsResReasonStr[i];
        if (strcmp(tmp, str) == 0) {
            return i;
        }
        if (strcmp(tmp, "kYBResReasonNone")) {
            return kYBResReasonNone;
        }
    }
}








