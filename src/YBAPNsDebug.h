//
//  YBAPNsDebug.h
//  YBApns2
//
//  Created by 云巴pro on 16/8/17.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBAPNsDebug_h
#define YBAPNsDebug_h

#ifdef __cplusplus
extern "C" {
#endif

#define YB_APNS_LOG_FLAG 0

#define YB_APNS_DEBUG_FLAG 1

#define YB_APNS_NULL_HANDLE_FLAG 1

// debug宏，用于调试时跟踪错误
#define YB_APNs_DEBUG(fmt,...) if(YB_APNS_DEBUG_FLAG) { fprintf(stderr, "[YBError]:%s():"fmt"\n",__func__,__VA_ARGS__); }

// log宏，用于调试的时候跟踪程序执行顺序
#define YB_APNs_LOG(fmt,...) if(YB_APNS_LOG_FLAG) { fprintf(stderr, fmt"\n",__VA_ARGS__); }

// NULL处理宏，用于简化参数为NULL的处理代码，便于维护
#define YB_APNs_NULL_HANDLE(OBJ,HANDLE,fmt,...) \
    if(YB_APNS_NULL_HANDLE_FLAG) { \
        if(OBJ == NULL) { \
            YB_APNs_DEBUG(fmt,__VA_ARGS__); \
            HANDLE \
        }\
    }
    
#ifdef __cplusplus
}
#endif



#endif /* YBAPNsDebug_h */
