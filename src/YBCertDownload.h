//
//  YBCertDownload.h
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/9.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBCertDownload_h
#define YBCertDownload_h

#include <stdio.h>
#include <curl/curl.h>

struct YB_cert_download_connection {
    CURL *curl;
};

extern int cert_exist(const char *path);

extern char * cert_download_url(const char *appkey,
                         const char *bundleId,
                         const char *type);

extern char * cert_download_storePath(const char *appkey,
                               const char *bundleId,
                                    const char *type);

extern char * cert_download_storeTimestamp(const char *appkey,
                                    const char *bundleId,
                                           const char *type);
extern int cert_download_verify_timeout(const char *timestampPath,uint32_t timeval);

extern int cert_download_write_timestamp(const char *ts);

extern int YB_cert_download_connection_create(struct YB_cert_download_connection *conn);

extern int YB_cert_download(struct YB_cert_download_connection *conn, const char * url,char *fp,char *ts);

extern void YB_cert_download_cleanning_up(struct YB_cert_download_connection *conn);

#endif /* YBCertDownload_h */
