//
//  YBAPNsRequest.c
//  YBApns2
//
//  Created by 云巴pro on 16/8/15.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdlib.h>
#include <string.h>
#include "YBAPNsRequest.h"
#include "YBAPNsDebug.h"


const char * const YBAPNsRequestAttributeStr[] = {
    "deviceToken",
    "apnsId",
    "apnsExpiration",
    "apnsTopic"
};

const char * const YBAPNsRequestPayloadStr[] = {
    "alert",
    "badge",
    "sound",
    "bodyStr",
};

const char * const YBAPNsRequestCerStr[] = {
    "kYBAPNsRequestCerType",
    "kYBAPNsRequestCerPath",
    "kYBAPNsRequestCerPsw"
};

const char * const YBAPNsRequestMethodStr[] = {
    "POST"
};

struct YBAPNsRequestObject {
    YBAPNsRequestHeader *header;
    YBAPNsRequestBody *body;
    const char * cerType;
    const char * cerPath;
    const char * cerPsw;
    int flag;
};

struct YBAPNsRequestHeader {
    YBAPNsRequestMethodType method; // should be "POST"
    const char * deviceToken;
    const char * apnsId;
    long apnsExpiration;
    YBAPNsReqPriority priority;
    const char * topic;
    int flag;
}; /* request header structure */

struct YBAPNsRequestBody {
    const char * alert; // notification content here
    uint32_t badge;
    const char * sound;
    const char * bodyStr;
    int flag;
}; /* request body sturcture  max size is 4KB */


static void free_if_not_null(const char *target) {
    if (target) {
        free((void *)target);
    }
}

void YB_request_setFlag(YBAPNsRequestObject *object,int flag) {
    YB_APNs_NULL_HANDLE(object, {return;}, "object is (NULL),set flag failed",NULL);
    object->flag = flag;
}

YBAPNsRequestObject * YB_request_object_init(YBAPNsRequestHeader * const header,YBAPNsRequestBody * const body,const char *cerType,const char *cerPath,const char *cerPsw) {
    YB_APNs_LOG("==>start create request object...",NULL);
    YBAPNsRequestObject *obj = (YBAPNsRequestObject *)calloc(1, sizeof(YBAPNsRequestObject));
    YB_APNs_NULL_HANDLE(obj, {return NULL;}, "request object calloc failed",NULL);
    YB_APNs_NULL_HANDLE(header, {return NULL;}, "header parameter is (NULL),create failed",NULL);
    YB_APNs_NULL_HANDLE(body, {return  NULL;}, "body parameter is (NULL),create failed",NULL);
    if (!YB_request_setHeader(obj, header) ||
        !YB_request_setBody(obj, body) ||
        !YB_request_setCer(obj,cerType, cerPath, cerPsw)) {
        YB_APNs_LOG("<==header or body or cer set failed\n",NULL);
        return NULL; 
    }
    obj->flag = 0;
    YB_APNs_LOG("<==request object create success\n",NULL);
    return obj;
}

YBAPNsRequestHeader * YB_request_header_init(YBAPNsRequestMethodType method,const char *deviceToken, const char *apnsId,long apnsExpiration,YBAPNsReqPriority priotiy,const char *apnsTopic) {
    YB_APNs_LOG("==>start create header object...",NULL);
    YBAPNsRequestHeader *header = (YBAPNsRequestHeader *)calloc(1, sizeof(YBAPNsRequestHeader));
    YB_APNs_NULL_HANDLE(header, {return NULL;}, "header calloc failed",NULL);
    if (!YB_request_setMethod(header, method)) {YB_request_setMethod(header,kYBAPNsRequestMethodTypePOST);};
    YB_request_setAttribute(header, deviceToken, 0, kYBAPNsRequestAttributeDeviceToken);
    YB_request_setAttribute(header, apnsId, 0, kYBAPNsRequestAttributeApnsId);
    YB_request_setAttribute(header, NULL, apnsExpiration, kYBAPNsRequestAttributeApnsExpiration);
    YB_request_setPriority(header, priotiy);
    YB_request_setAttribute(header, apnsTopic, 0, kYBAPNsRequestAttributeApnsTopic);
    header->flag = 0;
    YB_APNs_LOG("<==header object create success\n",NULL);
    return header;
}


YBAPNsRequestBody * YB_request_body_init(const char *alert,uint32_t badge,const char *sound,const char *bodyStr) {
    YB_APNs_LOG("==>start create body object...",NULL);
    YBAPNsRequestBody *body = (YBAPNsRequestBody *)calloc(1, sizeof(YBAPNsRequestBody));
    YB_APNs_NULL_HANDLE(body, {return NULL;}, "body calloc failed",NULL);
    if (bodyStr) {
        YB_request_setPayload(body, bodyStr, 0, kYBAPNsRequestPayloadBodyStr);
        body->alert = NULL;
        body->badge = 0;
        body->sound = NULL;
        YB_APNs_LOG("->bodyStr is not NULL,use bodyStr as a json content",NULL);
    }else {
        YB_request_setPayload(body, alert, 0, kYBAPNsRequestPayloadAlert);
        YB_request_setPayload(body, NULL, badge, kYBAPNsRequestPayloadBadge);
        YB_request_setPayload(body, sound, 0, kYBAPNsRequestPayloadSound);
    }
    body->flag = 0;
    YB_APNs_LOG("<==body object create success\n",NULL);
    return body;
}

void YB_request_object_clean(YBAPNsRequestObject *request) {
    YB_APNs_LOG("==>start clean request object:%p...",request);
    YB_APNs_NULL_HANDLE(request, {return;}, "request is NULL,clean failed",NULL);
    if (request->flag == 1) {
        YB_APNs_DEBUG("clean failed,request is set to a connection,you should clean the connection",NULL);
        return;
    }
    request->header->flag = 0;
    YB_request_header_clean(request->header);
    request->body->flag = 0;
    YB_request_body_clean(request->body);
    free_if_not_null(request->cerPath);
    free_if_not_null(request->cerPsw);
    free_if_not_null(request->cerType);
    free(request);
    YB_APNs_LOG("<==request clean success\n",NULL);
}
void YB_request_header_clean(YBAPNsRequestHeader *header) {
    YB_APNs_LOG("->start clean header object:%p...",header);
    YB_APNs_NULL_HANDLE(header, {return;}, "header is NULL,header clean failed",NULL);
    if (header->flag == 1) {
        YB_APNs_DEBUG("header is set to a request object,you should clean the object",NULL);
        return;
    }
    free_if_not_null(header->deviceToken);
    free_if_not_null(header->apnsId);
    free_if_not_null(header->topic);
    free(header);
    YB_APNs_LOG("<-clean header object success",NULL);
}
void YB_request_body_clean(YBAPNsRequestBody *body) {
    YB_APNs_LOG("->start clean body object:%p...",body);
    YB_APNs_NULL_HANDLE(body, {return;}, "clean failed body is NULL",NULL);
    if (body->flag == 1) {
        YB_APNs_DEBUG("body is set to a request object,you should clean the object",NULL);
        return;
    }
    free_if_not_null(body->alert);
    free_if_not_null(body->sound);
    free_if_not_null(body->bodyStr);
    free(body);
    YB_APNs_LOG("<-clean body object success",NULL);
}


YBAPNsRequestHeader * YB_request_getHeader(YBAPNsRequestObject * const object) {
    YB_APNs_NULL_HANDLE(object, {return NULL;}, "request object is (NULL),return NULL",NULL);
    YB_APNs_LOG("get request header:%p from object:%p",object->header,object);
    return object->header;
}

YBAPNsRequestBody * YB_request_getBody(YBAPNsRequestObject * const object) {
    YB_APNs_NULL_HANDLE(object, {return NULL;}, "request object is (NULL)",NULL);
    YB_APNs_LOG("get request body:%p from object:%p",object->body,object);
    return object->body;
}

int YB_request_getFlag(YBAPNsRequestObject *object) {
    YB_APNs_NULL_HANDLE(object, {return -1;}, "get flag failed,object is (NULL)",NULL);
    YB_APNs_LOG("get flag:%d",object->flag);
    return object->flag;
}


char * YB_request_get_str_Attribute(YBAPNsRequestHeader * const header,YBAPNsRequestAttribute key) {
    const char *attribute = NULL;
    YB_APNs_NULL_HANDLE(header, {return NULL;}, "header is (NULL), return NULL",NULL);
    switch (key) {
        case kYBAPNsRequestAttributeDeviceToken:
            attribute = header->deviceToken;
            break;
        case kYBAPNsRequestAttributeApnsId:
            attribute = header->apnsId;
            break;
        case kYBAPNsRequestAttributeApnsTopic:
            attribute = header->topic;
            break;
        default: {
            YB_APNs_DEBUG("no attach,key:%d,return NULL,please try to call YB_request_get_long_attribute func",key);
            return NULL;
        }
    }
    char * copy = NULL;
    if (attribute) {
       copy = strdup(attribute);
    }
    YB_APNs_LOG("get {%s:%s}",YBAPNsRequestAttributeStr[key],attribute);
    return copy;
}

long YB_request_get_long_attribute(YBAPNsRequestHeader * const header,YBAPNsRequestAttribute key) {
    YB_APNs_NULL_HANDLE(header, {return 0;}, "header is (NULL),return NULL",NULL);
    long attribute;
    switch (key) {
        case kYBAPNsRequestAttributeApnsExpiration:
            attribute = header->apnsExpiration;
            break;
        default: {
            YB_APNs_DEBUG("no attach,key:%d，return NULL,please try to use YB_request_get_str_Attribute func",key);
            attribute = 0;
            return attribute;
        }
            break;
    }
    YB_APNs_LOG("get {%s:%ld}",YBAPNsRequestAttributeStr[key],attribute);
    return attribute;
}

YBAPNsReqPriority YB_request_getApnsPriority(YBAPNsRequestHeader * const header) {
    YB_APNs_NULL_HANDLE(header, {return kYBReqPriorityNone;}, "header is (NULL),return kYBReqPriorityNone",NULL);
    YB_APNs_LOG("get {priority:%d}",header->priority);
    return header->priority;
}

YBAPNsRequestMethodType YB_request_getAPNsMethod(YBAPNsRequestHeader * const header) {
    YB_APNs_NULL_HANDLE(header, {return kYBAPNsRequestMethodTypeNone;}, "header is (NULL),return kYBAPNsRequestMethodTypeNone",NULL);
    YB_APNs_LOG("get {method:%d}",header->method);
    return header->method;
}

char * YB_request_getCer(YBAPNsRequestObject * const request,YBAPNsRequestCer type) {
    YB_APNs_NULL_HANDLE(request, {return NULL;}, "get certificate value failed,request is (NULL)",NULL);
    const char * str = NULL;
    if (type == kYBAPNsRequestCerPath) {
        str = request->cerPath;
    }else if (type == kYBAPNsRequestCerPsw) {
        str =  request->cerPsw;
    }else if (type == kYBAPNsRequestCerType) {
        str = request->cerType;
    }else {
        YB_APNs_DEBUG("type param is not attach key:%d,return NULL",type);
        return NULL;
    }
    char * copy = NULL;;
    if (str) {
        copy = strdup(str);
    }
    YB_APNs_LOG("get {%s:%s}",YBAPNsRequestCerStr[type],copy);
    return copy;
}

int YB_request_setHeader(YBAPNsRequestObject *object,YBAPNsRequestHeader *header) {
    YB_APNs_NULL_HANDLE(object, {return 0;}, "request object is (NULL),set failed",NULL);
    YB_APNs_NULL_HANDLE(header, {return 0;}, "header is (NULL),set failed",NULL);
    if (header->flag == 1) {
        YB_APNs_DEBUG("set failed,header:%p has been set to an object",header);
        return 0;
    }
    if (object->header) {
        object->header->flag = 0;
        YB_request_header_clean(object->header);
    }
    YB_APNs_LOG("set header:%p to request:%p",header,object);
    object->header = header;
    object->header->flag = 1;
    return 1;
}

int YB_request_setBody(YBAPNsRequestObject *object,YBAPNsRequestBody *body) {
    YB_APNs_NULL_HANDLE(object, {return 0;}, "request object is (NULL),set failed",NULL);
    YB_APNs_NULL_HANDLE(body, {return 0;}, "body is (NULL),set failed",NULL);
    if (body->flag == 1) {
        YB_APNs_DEBUG("set failed,body:%p has been set to an object,",body);
        return 0;
    }
    if (object->body) {
        object->body->flag = 0;
        YB_request_body_clean(object->body);
    }
    YB_APNs_LOG("set body:%p to request:%p",body,object);
    object->body = body;
    object->body->flag = 1;
    return 1;
}

int YB_request_setCer(YBAPNsRequestObject *object,const char * type,const char * path,const char * psw) {
    YB_APNs_NULL_HANDLE(object, {return 0;}, "request object is (NULL),return 0",NULL);
    if (!path || !type) {
        YB_APNs_DEBUG("type:%s path:%s, path/psw/type must not NULL",type,path);
        return 0;
    }
    YB_APNs_LOG("set certificate,{%s,%s,%s}",type,path,psw);
    object->cerPath = strdup(path);
    if (psw) {
        object->cerPsw = strdup(psw);
    }
    object->cerType = strdup(type);
    return 1;
}

int YB_request_setAttribute(YBAPNsRequestHeader * const header,const char * strAttribute,long longAttribute,YBAPNsRequestAttribute key) {
    YB_APNs_NULL_HANDLE(header, {return 0;}, "request header is (NULL),return NULL",NULL);
    char *copy = NULL;
    if (strAttribute) {
        copy = strdup(strAttribute);
    }
    switch (key) {
#warning 防止内存泄漏的free增加，若长时间没崩溃，可删除warnning
        case kYBAPNsRequestAttributeDeviceToken:
            free_if_not_null(header->deviceToken);
            header->deviceToken = copy;
            break;
        case kYBAPNsRequestAttributeApnsId:
            free_if_not_null(header->apnsId);
            header->apnsId = copy;
            break;
        case kYBAPNsRequestAttributeApnsTopic:
            free_if_not_null(header->topic);
            header->topic = copy;
            break;
        case kYBAPNsRequestAttributeApnsExpiration: {
            if (longAttribute > MAXIMUN_APNS_EXPIRATION_SIZE || longAttribute < MINIMUM_APNS_EXPIRATION_SIZE) {
                YB_APNs_DEBUG("apnsExpiration overflow}",NULL);
                return 0;
            }else {
                header->apnsExpiration = longAttribute;
                YB_APNs_LOG("set {%s:%ld}",YBAPNsRequestAttributeStr[key],longAttribute);
            }
            free_if_not_null(copy);
            return 1;
        }
        default: {
            YB_APNs_LOG("key not attach,key:%d",key);
            free_if_not_null(copy);
            return 0;
        }
    }
    if (key != kYBAPNsRequestAttributeApnsExpiration) {
        
    }
    YB_APNs_LOG("set {%s:%s}",YBAPNsRequestAttributeStr[key],strAttribute);
    return 1;
}

int YB_request_setPriority(YBAPNsRequestHeader * const header,YBAPNsReqPriority priority) {
    YB_APNs_NULL_HANDLE(header, {return 0;}, "request header is (NULL),return 0",NULL);
    switch (priority) {
        case kYBReqPriorityHigh:
        case kYBReqPriorityLow:
            break;
        default: {
            YB_APNs_DEBUG("priority no attach, key %d set priority to kYBReqPriorityHigh",priority);
            header->priority = kYBReqPriorityHigh;
            return 0;
        }
    }
    YB_APNs_LOG("set {priority:%d}",priority);
    header->priority = priority;
    return 1;
    
}

int YB_request_setMethod(YBAPNsRequestHeader * const header,YBAPNsRequestMethodType type) {
    YB_APNs_NULL_HANDLE(header, {return 0;}, "request header is (NULL),return 0",NULL);
    switch (type) {
        case kYBAPNsRequestMethodTypePOST:
            break;
        default: {
            YB_APNs_DEBUG("type no attach,key:%d",type);
            return 0;
        }
    }
    YB_APNs_LOG("set {method:%s}",YBAPNsRequestMethodStr[type]);
    header->method = type;
    return 1;
}

int YB_request_setPayload(YBAPNsRequestBody * const body,const char * strPayload,uint32_t intPayload,YBAPNsRequestPayload key) {
    YB_APNs_NULL_HANDLE(body, {return 0;}, "request body is (NULL),retrun 0",NULL);
    char * copy = NULL;
    if (strPayload) {
        copy = strdup(strPayload);
    }
    switch (key) {
        case kYBAPNsRequestPayloadAlert:
            free_if_not_null(body->alert);
            body->alert = copy;
            break;
        case kYBAPNsRequestPayloadSound:
            free_if_not_null(body->sound);
            body->sound = copy;
            break;
        case kYBAPNsRequestPayloadBodyStr:
            free_if_not_null(body->bodyStr);
            body->bodyStr = copy;
            break;
        case kYBAPNsRequestPayloadBadge: {
            body->badge = intPayload;
            YB_APNs_LOG("set payload:%d key:%s",intPayload,YBAPNsRequestPayloadStr[key]);
        }
            free_if_not_null(copy);
            return 1;
        default: {
            YB_APNs_LOG("key no attach,key:%d",key);
            free_if_not_null(copy);
            return 0;
        }
    }
    YB_APNs_LOG("set payload:%s key:%s",strPayload,YBAPNsRequestPayloadStr[key]);
    return 1;
}

char * YB_request_getPayload(YBAPNsRequestBody * const body,YBAPNsRequestPayload key) {
    YB_APNs_NULL_HANDLE(body, {return NULL;}, "request BODY is (NULL),return NULL",NULL);
    const char * attribute = NULL;
    switch (key) {
        case kYBAPNsRequestPayloadAlert:
            attribute = body->alert;
            break;
        case kYBAPNsRequestPayloadBodyStr:
            attribute = body->bodyStr;
            break;
        case kYBAPNsRequestPayloadSound:
            attribute = body->sound;
            break;
        default: {
            YB_APNs_LOG("key no attach,key:%d,please try to use YB_request_getIntPayload func",key);
            return NULL;
        }
    }
    YB_APNs_LOG("get payload:%s key:%s",attribute,YBAPNsRequestPayloadStr[key]);
    char *copy = NULL;
    if (attribute) {
        copy = strdup(attribute);
    }
    return copy;
}
uint32_t YB_request_getIntPayload(YBAPNsRequestBody * const body,YBAPNsRequestPayload key) {
    YB_APNs_NULL_HANDLE(body, {return 0;}, "request body is (NULL),return NULL",NULL);
    uint32_t value;
    switch (key) {
        case kYBAPNsRequestPayloadBadge:
            value = body->badge;
            break;
        default: {
            YB_APNs_LOG("key no attach,key:%d,return -1,please try to use YB_request_getPayload func",key);
            value = 0;
            return value;
        }
    }
    YB_APNs_LOG("get payload:%d key:%s",value,YBAPNsRequestPayloadStr[key]);
    return value;
}









