//
//  YBCertDownload.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/9.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include "YBCertDownload.h"
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <string.h>
#include <sys/stat.h>
#include "YBAPNsDebug.h"


struct FtpFile {
    const char *filename;
    FILE *stream;
};

// 检查证书的存在性
int cert_exist(const char *path) {
    
    if (access(path, R_OK) == -1) {
        perror("cert_exist() access error");
        return 0;
    }
    return 1;
}

// 证书下载地址
char * cert_download_url(const char *appkey,
                                const char *bundleId,
                                const char *type) {
    char *url = (char *)calloc(1, strlen("http://yunba.io/publicApi/app/")+
                               strlen("/ioscert/") +
                               strlen("/")+
                               strlen(appkey)+
                               strlen(bundleId)+
                               strlen(type) +1 );
    sprintf(url, "http://yunba.io/publicApi/app/%s/ioscert/%s/%s",appkey,bundleId,type);
    return url;
}
// 证书存储地址
char * cert_download_storePath(const char *appkey,
                                      const char *bundleId,
                                      const char *type) {
    char *path = (char *)calloc(1, strlen("cert/")+
                                strlen(appkey)+
                                strlen("_")+
                                strlen(bundleId)+
                                strlen("_")+
                                strlen(type)+
                                strlen("_")+
                                strlen("apns.pem") + 1);
    sprintf(path, "cert/%s_%s_%s_apns.pem",appkey,bundleId,type);
    return path;
}

char * cert_download_storeTimestamp(const char *appkey,
                                           const char *bundleId,
                                           const char *type) {
    char *path = (char *)calloc(1, strlen("cert/")+
                                strlen(appkey)+
                                strlen("_")+
                                strlen(bundleId)+
                                strlen("_")+
                                strlen(type)+
                                strlen("_")+
                                strlen("apns.pem") + 1);
    sprintf(path, "cert/%s_%s_%s_apns.stmp",appkey,bundleId,type);
    return path;
}

int cert_download_verify_timeout(const char *timestampPath,uint32_t timeval) {
    cert_exist(timestampPath);
    FILE *fp;
    long fs;
    time_t pre,now;
    char buf[100];
    fp = fopen(timestampPath, "rt");
    if (!fp) {
        YB_APNs_LOG("timestamp do not exist : %s",timestampPath);
        return 1;
    }
    fseek( fp , 0 , SEEK_END );
    fs = ftell(fp);
    fseek( fp , 0 , SEEK_SET);
    fread(buf, fs, 1, fp);
    fclose(fp);
    pre = atoi(buf);
    now = time(NULL);
    if (now - pre > timeval) {
        return 1;
    }
    return 0;
}

// download callback
static size_t
my_fwrite(void *buffer, size_t size, size_t nmemb, void *stream)
{
    struct FtpFile *out=(struct FtpFile *)stream;
    if(out && !out->stream) {
        
        out->stream=fopen(out->filename, "wb");//打开文件进行写入
        if(!out->stream)
            return -1;
    }
    return fwrite(buffer, size, nmemb, out->stream);
}


int YB_cert_download_connection_create(struct YB_cert_download_connection *conn) {
    mkdir("cert", 0777);
    CURL *curl;
    
    curl = curl_easy_init();  //初始化一个curl指针
    if(curl) {
        //下载回调
        curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, my_fwrite);
        curl_easy_setopt(curl, CURLOPT_VERBOSE, 1L);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYPEER, 0);
        curl_easy_setopt(curl, CURLOPT_SSL_VERIFYHOST, 0);
        //curl_easy_setopt(curl, CURLOPT_USERPWD, "SUREN:SUREN");
        conn->curl = curl;
        return 1;
    }
    conn->curl = NULL;
    return 0;
}

int cert_download_write_timestamp(const char *ts) {
    FILE *fp;
    char buf[100];
    time_t current;
    
    fp = fopen(ts, "wb"); // 打开文件进行写入
    if (!fp) {
        YB_APNs_DEBUG("open file failed",NULL);
        return 0;
    }
    current = time(NULL);
    sprintf(buf, "%ld",current);
    fwrite(buf, strlen(buf), 1, fp);
    fclose(fp);
    return 1;
}

void YB_cert_download_cleanning_up(struct YB_cert_download_connection *conn) {
    if (conn->curl) {
        curl_easy_cleanup(conn->curl);
    }
}


int YB_cert_download(struct YB_cert_download_connection *conn, const char * url,char *fp,char *ts) {
    CURLcode res;
    CURL *curl = conn->curl;
    int flag = 0;
    char *filePath = strdup(fp);
    char *fileurl = strdup(url);
    struct FtpFile ftpfile={
        filePath, //定义下载到本地的文件位置和路径
        NULL
    };
    if (curl) {
        // 地址
        curl_easy_setopt(curl, CURLOPT_URL,fileurl);
        curl_easy_setopt(curl, CURLOPT_WRITEDATA, &ftpfile);
        //写入
        res = curl_easy_perform(curl);
        flag = 1;
        if(res != CURLE_OK) {
            flag = 0;
            YB_APNs_DEBUG("cert download failed",NULL);
        }
    }
    if (flag) {
        if (!cert_download_write_timestamp(ts)) {
            YB_APNs_DEBUG("write cert timestamp failed",NULL);
        }
    }
    if(ftpfile.stream) { fclose(ftpfile.stream); }
    free(filePath); free(fileurl);
    if (flag)
        return 1;
    else
        return 0;
}