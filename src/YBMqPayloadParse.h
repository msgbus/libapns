//
//  YBMqPayloadParse.h
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/12.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBMqPayloadParse_h
#define YBMqPayloadParse_h

#include <stdio.h>


// rabbitmq -> data -> mpayload
struct mqPayload {
    char *uid;
    char *alert;
    uint32_t badge;
    char *sound;
    char *bodyStr;
    int isBodyStr;
};


extern void * uInt_parse(const void * src, int len);

extern char * str_parse(const void * src, int len);

extern struct mqPayload payload_parse(const void * payload);

#endif /* YBMqPayloadParse_h */
