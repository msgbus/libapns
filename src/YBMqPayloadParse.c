//
//  YBMqPayloadParse.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/12.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdlib.h>
#include <string.h>

#include "YBMqPayloadParse.h"




// void * -> uint32_t/uint64_t 暂时只适用于小端机器
void * uInt_parse(const void * src, int len) {
    char *dt = (char *)calloc(1, len);
    for (int i = len - 1; i >= 0; i--) {
        dt[len - 1 - i] = ((char *)src)[i];
    }
    return dt;
}
// void * -> char * 解析
char * str_parse(const void * src, int len) {
    char *str = (char *)calloc(1, len +1);
    for (int i = 0; i < len; i++) {
        str[i] = ((char *)src)[i];
    }
    str[len] = '\0';
    return str;
}


static void uid_expiry_parse(struct mqPayload *mp,void *uid,void *expiry) {
    char buf[80];
    uint64_t *uidInt;
    char *uidStr;
    uint32_t *expiryInt;
    
    // parse uid
    uidInt = uInt_parse(uid, 8);
    sprintf(buf, "%llu",*uidInt);
    size_t size = strlen(buf);
    uidStr = (char *)calloc(1, size + 1);
    snprintf(uidStr, size + 1, "%s",buf);
    // parse expiry
    expiryInt = uInt_parse(expiry, 4);
    mp->uid = uidStr;
    
    free(expiryInt);free(uidInt);
}

static void content_parse(struct mqPayload *mp,void *payloadContent,uint32_t payloadLen,int isBodyStr) {
    char *plContentStr;
    
    plContentStr = str_parse(payloadContent, payloadLen);
    // parse alert content
    if (isBodyStr) {
        mp->bodyStr = plContentStr;
        mp->isBodyStr = 1;
        
        mp->alert = NULL;
        mp->badge = 0;
        mp->sound = NULL;
    }else {
        mp->alert = plContentStr;
        mp->sound = strdup("default");
        mp->badge = 1;
        mp->isBodyStr = 0;
        
        mp->bodyStr = NULL;
    }
}
// rabbitmq payload 解析
struct mqPayload
payload_parse(const void * payload) {
    struct mqPayload mp;
    void * uid, *expiry;
    
    void *payloadLen;
    uint32_t *payloadLenInt;
    
    void *payloadContent;
    int isBodyStr;
    
    uid = (void *)calloc(1, 8);
    memcpy(uid, payload, 8);
    expiry = (void *)calloc(1, 4);
    memcpy(expiry, payload+8, 4);
    
    // parse uid and expiry
    uid_expiry_parse(&mp,uid,expiry);
    
    // check payloadlen type,if it is zero,
    // the payload is the new payload type,use bodyStr.
    payloadLen = (void *)calloc(1, 2);
    memcpy(payloadLen, payload+8+4, 2);
    payloadLenInt = uInt_parse(payloadLen, 2);
    if(*payloadLenInt == 0) {
        // free old memory
        free(payloadLen);free(payloadLenInt);
        payloadLen  = (void *)calloc(1, 2);
        memcpy(payloadLen, payload+8+4+2+1, 2);
        payloadLenInt =  uInt_parse(payloadLen, 2);
        isBodyStr = 1;
        
        payloadContent = (void *)calloc(1, *payloadLenInt + 1);
        memcpy(payloadContent, payload+8+4+2+1+2, *payloadLenInt + 1);
    }else {
        payloadContent = (void *)calloc(1, *payloadLenInt + 1);
        memcpy(payloadContent, payload+8+4+2, *payloadLenInt + 1);
        isBodyStr = 0;
    }
    // parse content
    content_parse(&mp,payloadContent,*payloadLenInt,isBodyStr);
    
    //free deprecated memory
    free(uid);free(expiry);
    free(payloadLen);free(payloadLenInt);
    free(payloadContent);
    
    return mp;
}