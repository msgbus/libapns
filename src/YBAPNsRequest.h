//
//  YBAPNsRequest.h
//  YBApns2
//
//  Created by 云巴pro on 16/8/15.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBAPNsRequest_h
#define YBAPNsRequest_h

#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <stdint.h>
/**
 *  request body 的最大值 4KB
 */
#define MAXIMUN_APNS_REQUEST_BODY_SIZE 4096
/**
 *  apns-expiration 的取值范围 在 -2^32 ~ 2^32 - 1 之间
 */
#define MINIMUM_APNS_EXPIRATION_SIZE -2147483648L
#define MAXIMUN_APNS_EXPIRATION_SIZE 2147483647L

/*
 * 用来设置request header 的属性
 */
typedef enum YBAPNsRequestAttribute {
    kYBAPNsRequestAttributeDeviceToken,
    kYBAPNsRequestAttributeApnsId,
    kYBAPNsRequestAttributeApnsExpiration, // set the ApnsExpiration,the value should be limited in MINIMUM_APNS_EXPIRATION_SIZE and MAXIMUN_APNS_EXPIRATION_SIZE
    kYBAPNsRequestAttributeApnsTopic //  always be bundle ID
}YBAPNsRequestAttribute;

// 求 enum 的 字面值
extern const char * const YBAPNsRequestAttributeStr[];/* use YBAPNsRequestAttributeStr[kYBAPNsRequestAttributeXXX] to search string for enum YBAPNsRequestAttribute*/

/*
 * 设定 request method 字段的可选值，这个值可以将来可以拓展
 */
typedef enum YBAPNsRequestMethodType {
    kYBAPNsRequestMethodTypePOST,
    kYBAPNsRequestMethodTypeNone
}YBAPNsRequestMethodType;

// 求 enum 的 字面值
extern const char * const YBAPNsRequestMethodStr[];

/*
 * 设定 证书 字段
 */
typedef enum YBAPNsRequestCer {
    kYBAPNsRequestCerType, // 类型如“pem”
    kYBAPNsRequestCerPath, // 证书的路径
    kYBAPNsRequestCerPsw // 证书密码
}YBAPNsRequestCer;

// 求 enum 的 字面值
extern const char * const YBAPNsRequestCerStr[];

/*
 * 设定 request body 的 json 可直接用自定义的json来设置bodySty，也可一个一个来。可以拓展以增加新的值。
 */
typedef enum YBAPNsRequestPayload {
    kYBAPNsRequestPayloadAlert,
    kYBAPNsRequestPayloadBadge,
    kYBAPNsRequestPayloadSound,
    kYBAPNsRequestPayloadBodyStr
}YBAPNsRequestPayload; /* set payload in request */

// 求 enum 的 字面值
extern const char * const YBAPNsRequestPayloadStr[];/* use YBAPNsRequestPayloadStr[kYBAPNsRequestPayloadXXX] to search string for enum YBAPNsRequestPayload*/

// 设定 apns-priority 字段 的 可选值
typedef enum YBAPNsReqPriority {
    kYBReqPriorityHigh = 10,
    kYBReqPriorityLow = 5,
    kYBReqPriorityNone = 0
}YBAPNsReqPriority; /* Apns-Priority */


typedef struct YBAPNsRequestObject YBAPNsRequestObject; /* request object */

typedef struct YBAPNsRequestHeader YBAPNsRequestHeader; /* header object */

typedef struct YBAPNsRequestBody YBAPNsRequestBody; /* body object */


/**
 * 初始化 函数
 */
// YBAPNsRequstOjbect
extern YBAPNsRequestObject * YB_request_object_init(YBAPNsRequestHeader * const header,YBAPNsRequestBody * const body,const char *cerType,const char *cerPath,const char *cerPsw);
// YBAPNsRequestHeader
extern YBAPNsRequestHeader * YB_request_header_init(YBAPNsRequestMethodType method,const char *deviceToken, const char *apnsId,long apnsExpiration,YBAPNsReqPriority priotiy,const char *apnsTopic);
// YBAPNsRequestBody
// 如果设置了 bodyStr 则 函数 会优先使用 bodyStr，如果 bodyStr 是 NULL，函数会初始化其他的值，最后会自动根据其他值创建 json
extern YBAPNsRequestBody * YB_request_body_init(const char *alert,uint32_t badge,const char *sound,const char *bodyStr);

/**
 * cleanup 函数
 */
extern void YB_request_object_clean(YBAPNsRequestObject *request);
extern void YB_request_header_clean(YBAPNsRequestHeader *header);
extern void YB_request_body_clean(YBAPNsRequestBody *body);


/**
 *  从 object 中获取 header 和 body
 */
extern YBAPNsRequestHeader * YB_request_getHeader(YBAPNsRequestObject * const object);
extern YBAPNsRequestBody * YB_request_getBody(YBAPNsRequestObject * const object);

// 获取 request 的 flag 值，如果为1，则表示 object 已经被 装入 YBAPNsConnection object 之中
extern int YB_request_getFlag(YBAPNsRequestObject *object);
// 获取 request 证书的 类型 路径 密码
extern char * YB_request_getCer(YBAPNsRequestObject * const request,YBAPNsRequestCer type);

/*
 * 从 header 之中获取 相应 key 字段中的 值
 */
extern char * YB_request_get_str_Attribute(YBAPNsRequestHeader * const header,YBAPNsRequestAttribute key);
extern long YB_request_get_long_attribute(YBAPNsRequestHeader * const header,YBAPNsRequestAttribute key);
// 直接 从 header 之中 获取 priority 值
extern YBAPNsReqPriority YB_request_getApnsPriority(YBAPNsRequestHeader * const header);
// 直接 从 header 之中 获取 method 值
extern YBAPNsRequestMethodType YB_request_getAPNsMethod(YBAPNsRequestHeader * const header);


/*
 * 从 body 之中获取 相应 key 字段中的 值
 */
extern char * YB_request_getPayload(YBAPNsRequestBody * const body,YBAPNsRequestPayload key);
extern uint32_t YB_request_getIntPayload(YBAPNsRequestBody * const body,YBAPNsRequestPayload key);


/**
 *  设置 request 的 header,body 和 证书
 */
extern int YB_request_setHeader(YBAPNsRequestObject *object,YBAPNsRequestHeader *header);
extern int YB_request_setBody(YBAPNsRequestObject *object,YBAPNsRequestBody *body);
extern int YB_request_setCer(YBAPNsRequestObject *object,const char * type,const char * path,const char * psw);

/*
 * 设置 header 中 的 值， 
 * 若是 key 是 kYBAPNsRequestAttributeApnsExpiration，会使用 longAttribute 值作为参数，
 * 若是其他会默认使用 strAttribute 作为参数
 */
extern int YB_request_setAttribute(YBAPNsRequestHeader * const header,const char * strAttribute,long longAttribute,YBAPNsRequestAttribute key);
//直接 设置 header 的 priority
extern int YB_request_setPriority(YBAPNsRequestHeader * const header,YBAPNsReqPriority priority);
//直接 设置 header 的 method
extern int YB_request_setMethod(YBAPNsRequestHeader * const header,YBAPNsRequestMethodType type);

/*
 * 设置 body 的 payload
 * 如果 key 是 kYBAPNsRequestPayloadBadge，默认以 intPayload 为参数
 * 如果 key 是 其他，默认以 strPayload 为 参数
 */
extern int YB_request_setPayload(YBAPNsRequestBody * const body,const char * strPayload,uint32_t intPayload,YBAPNsRequestPayload key);



#ifdef __cplusplus
}
#endif

#endif /* YBAPNsRequest_h */
