//
//  YBAPNs.c
//  YBApns2
//
//  Created by 云巴pro on 16/8/16.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdlib.h>
#include <stdint.h>
#include <string.h>
#include <stdarg.h>
#include <unistd.h>
#include "YBAPNsDebug.h"
#include "YBAPNs.h"
#include "cJSON.h"


#ifndef CURLPIPE_MULTIPLEX
/* This little trick will just make sure that we don't enable pipelining for
 libcurls old enough to not have this symbol. It is _not_ defined to zero in
 a recent libcurl header. */
#define CURLPIPE_MULTIPLEX 0
#endif

/*
 * 用于合成字符串时给定第二个参数的类型
 */
typedef enum YBAPNsHeapStringOption {
    kYBAPNsHeapStringOptionStringParam,
    kYBAPNsHeapStringOptionIntParam
}YBAPNsHeapStringOption;

const char * const YBAPNsConfigStr[] = {
    "Server",
    "SubPath",
};

/*
 * YBAPNs的全局变量
 */
struct YBAPNs_config{
    const char *server;
    const char *subsPath;
    char *url;
    int globalFlag;
}YBAPNs_config;

/*
 * connection 核心对象
 */
struct YBAPNsConncetion {
    YBAPNsRequestObject * request;
    CURL *curl;
    struct curl_slist *slist;
    int verbose;
    char * bodyDataCache;
};
/*
 * YBAPNsRequest.c里面的一个私有函数，只暴露给本文件使用
 */
extern void YB_request_setFlag(YBAPNsRequestObject *object,int flag);

#pragma mark - private
/*
 * -----****************** 以下是私有函数 ***************************-----
 */

/*
 * 通过调用可以把两个str合成为str:str的键值对形式，注意得到的char*需要free。
 */
static char * KVString(const char * str,YBAPNsHeapStringOption type,const char *cparam,long lparam) {
    char * result;
    if (type == kYBAPNsHeapStringOptionStringParam) {
        size_t size = strlen(str)+strlen(cparam)+2;
        result = (char *)calloc(1, size);
        snprintf(result,size, "%s:%s",str,cparam);
    }else if (type == kYBAPNsHeapStringOptionIntParam) {
        char buf[80] = "";
        sprintf(buf,"%ld",lparam);
        size_t size = strlen(str)+strlen(buf)+2;
        result = (char *)calloc(1, size);
        snprintf(result,size, "%s:%s",str,buf);
    }else {
        return NULL;
    }
    return result;
}

/*
 * 通过调用可以把key关键字后面的size个字符提取出来，用于提取http响应header中的值，注意返回值需要free
 */
static char *YB_get_value_of_key(const char *content,const char *key,size_t size) {
    const char *tmp = content;
    char *result = NULL;
    if (strstr(tmp, key)) {
        tmp += strlen(key)+1;
        result = (char *)calloc(1, size);
        strncpy(result, tmp, size);
    }
    return result;
}

/*
 * http响应的body部分的处理回调
 */
static size_t YB_connection_http_body_callback(char *data,size_t size,size_t nitems,YBAPNsResponseObject *response) {
    long sum = size * nitems;
    // 解析数据
    cJSON *root = cJSON_Parse(data);
    if (root) {
        //reason 字段
        cJSON *reasonJson = cJSON_GetObjectItem(root, "reason");
        char *reason = NULL;
        if (reasonJson) {
            reason = reasonJson->valuestring;
        }
        if (reason) {
            response->reason = YB_response_search(reason);
        }else {
            response->reason = kYBResReasonNone;
        }
        // timestamp 字段
        cJSON *timestampJson = cJSON_GetObjectItem(root, "timestamp");
        char *timestamp = NULL;
        if (timestampJson) {
            timestamp = timestampJson->valuestring;
        }
        if (timestamp) {
            response->timestamp = timestamp;
        }
    }
    cJSON_Delete(root);
    return sum;
}

/*
 * http响应的header部分的处理回调
 */
static size_t YB_connection_http_header_callback(char *data,size_t size,size_t nitems,YBAPNsResponseObject *response) {
    long sum = size * nitems;
    // 解析数据
    //statusCode 字段
    char *statusCode = YB_get_value_of_key(data, "HTTP/2", 3);
    if (statusCode) {
        int code = atoi(statusCode);
        response->statusCode = code;
        free(statusCode);
    }
    // apnsId 字段
    char *apnsId = YB_get_value_of_key(data, "apns-id", 36);
    if (apnsId) {
        response->apnsId = apnsId;
    }
    return sum;
}

/*
 * 针对全局变量YBAPNs_config的处理函数，该函数可以调用多次
 */
static void YB_connection_config_init() {
    if (!YBAPNs_config.server) {
        YBAPNs_config.server = kYBDefaultAPNsServerAddr;
    }
    if (!YBAPNs_config.subsPath) {
        YBAPNs_config.subsPath = kYBDefaultAPNsServerSubPath;
    }
    if (YBAPNs_config.url) {
        free(YBAPNs_config.url);
    }
    size_t size = strlen(YBAPNs_config.server)+strlen(YBAPNs_config.subsPath) + 1;
    YBAPNs_config.url = (char *)calloc(1, size);
    snprintf(YBAPNs_config.url,size,"%s%s", YBAPNs_config.server,YBAPNs_config.subsPath);
}

/*
 * 通过调用，该函数将从connection中获取里面的header信息并形成curl_slist链表返回给调用者
 */
static void YB_get_header_slist(YBAPNsConnection * connection) {
    YB_APNs_LOG("-->create header list",NULL);
    struct curl_slist *slist = connection->slist;
    if (!slist) {
        slist = curl_slist_append(slist, "1");
        slist = curl_slist_append(slist, "2");
        slist = curl_slist_append(slist, "3");
    }
    YBAPNsRequestObject *request = connection->request;
    YBAPNsRequestHeader *header = YB_request_getHeader(request);
    
    // set apns-expiration header
    long expiration = YB_request_get_long_attribute(header, kYBAPNsRequestAttributeApnsExpiration);
    char *expiHeader = KVString("apns-expiration", kYBAPNsHeapStringOptionIntParam, NULL, expiration);
    if (slist->data) {
        free(slist->data);
    }
    if (expiHeader) {
        slist->data = expiHeader;
    }else
        slist->data = strdup("apns-expiration:10");
    
    // set apns-priority header
    YBAPNsReqPriority priority = YB_request_getApnsPriority(header);
    char *priorityHeader = KVString("apns-priority", kYBAPNsHeapStringOptionIntParam, NULL,priority);
    if (slist->next->data) {
        free(slist->next->data);
    }
    if (priorityHeader) {
        slist->next->data = priorityHeader;
    }else
        slist->next->data = strdup("apns-priority:10");
    
    // set apns-topic header
    char *apnsTopic = YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeApnsTopic);
    if (slist->next->next->data) {
        free(slist->next->next->data);
    };
    if (apnsTopic) {
        char *apnsTopicHeader = KVString("apns-topic", kYBAPNsHeapStringOptionStringParam, apnsTopic, 0);
        slist->next->next->data = apnsTopicHeader;
        free(apnsTopic);
    }else {
        slist->next->next->data = strdup("apns-topic:NoTopic");
    }
    
    // set apns-id header
//    char *apnsId = YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeApnsId);
//    if (apnsId) {
//        char *apansIdHeader = KVString("apns-id", kYBAPNsHeapStringOptionStringParam, apnsId, 0);
//        slist = curl_slist_append(slist, apansIdHeader);
//        free(apansIdHeader);free(apnsId);
//    }
    connection->slist = slist;
    YB_APNs_LOG("<--create header list success",NULL);
}

/*
 * 该函数集成了所有的curl_easy_setopt()函数调用，通过调用，会把connection和headerlist里面的相关信息设置到curl里
 */
static void YB_connection_setOption(const YBAPNsConnection * connection) {
    YB_APNs_LOG("-->create header option",NULL);
    if (!YBAPNs_config.url) {
        YB_connection_config_init();
    }
    CURL *curl = connection->curl;
    YBAPNsRequestObject *request = connection->request;
    YBAPNsRequestHeader *header = YB_request_getHeader(request);
    char *type = YB_request_getCer(request, kYBAPNsRequestCerType);
    char *path = YB_request_getCer(request,kYBAPNsRequestCerPath);
    char *psw = YB_request_getCer(request,kYBAPNsRequestCerPsw);
    char *deviceToken = YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeDeviceToken);
    if (connection->verbose == 1) {
        curl_easy_setopt(connection->curl, CURLOPT_VERBOSE,1);
    }else {
        curl_easy_setopt(connection->curl, CURLOPT_VERBOSE,0);
    }
    char * urlWithDeviceToken;
    if (deviceToken) {
        size_t size = strlen(YBAPNs_config.url)+strlen(deviceToken) + 2;
        urlWithDeviceToken = (char *)calloc(1, size);
        snprintf(urlWithDeviceToken,size, "%s/%s",YBAPNs_config.url,deviceToken);
        free(deviceToken);
        
    }else {
        urlWithDeviceToken = YBAPNs_config.url;
    }
    curl_easy_setopt(connection->curl, CURLOPT_URL,urlWithDeviceToken);
    curl_easy_setopt(connection->curl, CURLOPT_SSLCERTTYPE,type);
    curl_easy_setopt(connection->curl, CURLOPT_SSLCERT, path);
    if (psw) {
        curl_easy_setopt(connection->curl, CURLOPT_SSLCERTPASSWD,psw);
    }
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
    curl_easy_setopt(connection->curl, CURLOPT_POST,1);
    curl_easy_setopt(connection->curl, CURLOPT_HTTPHEADER,connection->slist);
    free(type); free(path); free(psw);
    
    YB_APNs_LOG("<--create header option success",NULL);
    
}

/*
 * 该函数设置curl_easy_setopt的body部分
 */
static void YB_connection_setOption_body(YBAPNsConnection *connection) {
    YB_APNs_LOG("-->create body option",NULL);
    YB_APNs_NULL_HANDLE(connection, {return;}, "connection is (NULL),set option failed",NULL);
    YBAPNsRequestObject *request = connection->request;
    YBAPNsRequestBody *body = YB_request_getBody(request);
    char *bodyStr = YB_request_getPayload(body, kYBAPNsRequestPayloadBodyStr);
    // 如果存在用户自定义的json
    if (bodyStr) {
        curl_easy_setopt(connection->curl, CURLOPT_POSTFIELDS,bodyStr);
    }else {
        // 否则开始创建json
        cJSON *json = cJSON_CreateObject();
        cJSON *aps = cJSON_CreateObject();
        cJSON_AddItemToObject(json, "aps", aps);
        char *alert =YB_request_getPayload(body, kYBAPNsRequestPayloadAlert);
        if (alert) {
            cJSON_AddStringToObject(aps, "alert", alert);
            free(alert);
        }
        uint32_t badge = YB_request_getIntPayload(body, kYBAPNsRequestPayloadBadge);
        if (badge != 0) {
            char *badgeStr = (char *)calloc(1, 50);
            snprintf(badgeStr, 50,"%d",badge);
            cJSON_AddStringToObject(aps, "badge", badgeStr);
            free(badgeStr);
        }
        char *sound = YB_request_getPayload(body, kYBAPNsRequestPayloadSound);
        if (sound) {
            cJSON_AddStringToObject(aps, "sound", sound);
            free(sound);
        }
        bodyStr = cJSON_PrintUnformatted(json);
        curl_easy_setopt(connection->curl, CURLOPT_POSTFIELDS,bodyStr);
        cJSON_Delete(json);
    }
    if (connection->bodyDataCache) {
        free(connection->bodyDataCache);
    }
    connection->bodyDataCache = bodyStr;
    YB_APNs_LOG("<--create body option success",NULL);
}

/*
 * 该函数设置curl_easy_setopt()函数回调的函数回调，回调数据将会写入response
 */
static void YB_connection_data_callback(YBAPNsConnection *connection,YBAPNsResponseObject *response) {
    YB_APNs_LOG("-->set data call back",NULL);
    CURL *curl = connection->curl;
    curl_easy_setopt(curl, CURLOPT_HEADERDATA,response);
    curl_easy_setopt(curl, CURLOPT_HEADERFUNCTION, YB_connection_http_header_callback);
    
    curl_easy_setopt(curl, CURLOPT_WRITEDATA, response);
    curl_easy_setopt(curl, CURLOPT_WRITEFUNCTION, YB_connection_http_body_callback);
    YB_APNs_LOG("<--set data call back success",NULL);
}



/*
 * -----****************** 以下是公有API ***************************-----
 */

#pragma mark - public
// 信号量的问题需要去解决
/*
 * 调用curl的全局函数curl_global_init()，该函数在一个application中应该只调用一次。
 */
void YB_connection_global_init() {
    YB_connection_config_init();
    if (!YBAPNs_config.globalFlag) {
        CURLcode code = curl_global_init(CURL_GLOBAL_ALL);
        if (code != CURLE_OK) {
            YB_APNs_DEBUG("curl_global_init() failedcurlcode:%d",code);
            return;
        }
        YB_APNs_LOG("global init is successful",NULL);
        YBAPNs_config.globalFlag = 1;
    }else
        YB_APNs_LOG("global init has been called,the func should be called once",NULL);
    return;
}

/*
 * 设置全局变量
 */
void YB_APNs_SetConfig(const char * config,YBAPNsConfig key) {
    YB_APNs_LOG("setConfig:{%s:%s}",YBAPNsConfigStr[key],config);
    switch (key) {
        case YBAPNsConfigServer:
            YBAPNs_config.server = config;
            break;
        case YBAPNsConfigSubPath:
            YBAPNs_config.subsPath = config;
            break;
        default:
            break;
    }
    YB_connection_config_init();
}

/*
 * 获取全局变量
 */
const char * YB_APNs_getConfig(YBAPNsConfig key) {
    const char * config;
    switch (key) {
        case YBAPNsConfigServer:
            config = YBAPNs_config.server;
            break;
        case YBAPNsConfigSubPath:
            config = YBAPNs_config.subsPath;
            break;
        default:
            YB_APNs_LOG("key no attach",NULL);
            break;
    }
    return config;
}


/*
 * 通过 YBAPNsRequestObject 初始化 YBAPNsConnection
 */
YBAPNsConnection * YB_connection_init(YBAPNsRequestObject * request) {
    YB_APNs_LOG("===>start create connection object...",NULL);
    YB_APNs_NULL_HANDLE(request, {return  NULL;}, "create failed,request object is NULL,return NULL,",NULL);
    YBAPNsConnection *connection = (YBAPNsConnection *)calloc(1, sizeof(YBAPNsConnection));
    YB_APNs_NULL_HANDLE(connection, {return  NULL;}, "connection calloc failed,stop init",NULL);
    CURL *curl = curl_easy_init();
    YB_APNs_NULL_HANDLE(curl, {
        free(connection);
        return NULL;
    }, "failed to create curl connection",NULL);
    if (!YB_connection_setHeader(connection, request)) {
        free(connection);
        curl_easy_cleanup(curl);
        return NULL;
    }
    connection->curl = curl;
    YB_APNs_LOG("<===connection create success\n",NULL);
    return connection;
}

/*
 * connection 的 cleanup，会一并清理 YBAPNsRequestObject
 */
void YB_connection_clean(YBAPNsConnection * connection) {
    YB_APNs_NULL_HANDLE(connection, {return;}, "clean failed,connection is (NULL)",NULL);
    YB_APNs_LOG("===>start clean connection:%p...",connection);
    if (connection->slist) {curl_slist_free_all(connection->slist);}
    curl_easy_cleanup(connection->curl);
    YB_request_setFlag(connection->request, 0);
    YB_request_object_clean(connection->request);
    free(connection);
    YB_APNs_LOG("<===connection clean success\n",NULL);
}
/*
 * 更换 connection 里面的 request ,会 cleanup
 */
int YB_connection_setHeader(YBAPNsConnection *connection,YBAPNsRequestObject * request) {
    YB_APNs_NULL_HANDLE(connection, {return 0;}, "set failed,connection is (NULL)",NULL);
    YB_APNs_NULL_HANDLE(request, {return 0;}, "request failed,request is (NULL)",NULL);
    if (YB_request_getFlag(request)) {
        YB_APNs_DEBUG("set failed,request:%p has been set to a connection",NULL);
        return 0;
    }
    if (connection->request) {
        YB_request_object_clean(request);
    }
    YB_APNs_LOG("set request:%p to connection:%p",request,connection);
    YB_request_setFlag(request, 1);
    connection->request = request;
    return 1;
    
}

YBAPNsRequestObject * YB_connection_getRequest(YBAPNsConnection * connection) {
    YB_APNs_NULL_HANDLE(connection, {return NULL;}, "connection is (NULL),get failed",NULL);
    YB_APNs_LOG("get request:%p from connection:%p",connection->request,connection);
    return connection->request;
}

/*
 * 该函数设置verbose，verbose = 1时，会输出apns发送的debug信息
 */
void YB_connection_setVerbose(YBAPNsConnection *connection,int verbose) {
    YB_APNs_NULL_HANDLE(connection, {return;}, "connection is (NULL),set verbose failed",NULL);
    YB_APNs_LOG("set verbose:%d to connection:%p",verbose,connection);
    connection->verbose = verbose;
}

/*
 * 该函数会把request里的值设置到curl里
 */
int YB_connection_curl_prepare(YBAPNsConnection * connection) {
    YB_APNs_LOG("==>start prepare a connection:%p",connection);
    YB_APNs_NULL_HANDLE(connection, {return 0;}, "connection is 0,perform failed",NULL);
    YB_get_header_slist(connection);
    YB_connection_setOption(connection);
    YB_connection_setOption_body(connection);
    
    YB_APNs_LOG("<==prepare a connection:%p",connection);
    return 1;
}
/*
 * 单链接顺序传输用此函数
 */
YBAPNsResponseObject * YB_connection_perform(YBAPNsConnection * connection) {
    YB_APNs_LOG("===>start perform a connection:%p",connection);
    YB_APNs_NULL_HANDLE(connection, {return NULL;}, "connection is (NULL),perform failed",NULL);
    
    if (!YB_connection_curl_prepare(connection)) {return NULL;}
    
    YB_APNs_LOG("-->create respone",NULL);
    YBAPNsResponseObject *response = (YBAPNsResponseObject *)calloc(1, sizeof(YBAPNsResponseObject));
    YB_APNs_NULL_HANDLE(response, {return NULL;}, "calloc failed,perform is stop",NULL);
    response->reason = kYBResReasonNone;
    YB_connection_data_callback(connection, response);
    YB_APNs_LOG("<--respone create success",NULL);
    
    YB_APNs_LOG("-->send message",NULL);
    CURLcode result = curl_easy_perform(connection->curl);
    if (result != CURLE_OK) {
        free(response);
        YB_APNs_LOG("<===connection perform failed\n",NULL);
        return NULL;
    }
    YB_APNs_LOG("<===connection perform success\n",NULL);
    return response;
}

/*
 * 获取一个默认设置的connection对象的便利函数，但是该connection 对象不能直接执行传输
 */
YBAPNsConnection * YB_connection_get_default_conn(int verbose) {
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "devToken", NULL, 10, kYBReqPriorityHigh, "bundleId");
    YBAPNsRequestBody *body = YB_request_body_init(NULL, 0, NULL, NULL);
    YBAPNsRequestObject *request = YB_request_object_init(header, body, "PEM", "cerPath",NULL);
    YBAPNsConnection *connection = YB_connection_init(request);
    YB_connection_setVerbose(connection,verbose);
    return connection;
}



#pragma mark - multiplexing API

/*
 * 该函数将剔除multi_handle里面的所有multi_curl，并且将每个multi_curl加锁的文件解锁并关闭
 */
static void remove_all_handle(struct YBAPNsmulti_curl_set *set) {
    for (int i = 0; i < set->len; i++) {
        if (set->multi_curl_set[i].added == 1) {
            YB_APNs_LOG("remove curl handle:%d(%p) used:%d",i,set->multi_curl_set[i].conn->curl,set->multi_curl_set[i].used);
            curl_multi_remove_handle(set->multi_handle, set->multi_curl_set[i].conn->curl);
            set->multi_curl_set[i].added = 0;
        }
    }
}

/*
 * 该函数将set里面所有的multi_curl的 使用位 置0，表示该multi_curl已经成为未使用的object
 */
static void set_used_zero(struct YBAPNsmulti_curl_set *set) {
    for (int i = 0; i < set->len; i ++) {
        if (1 == set->multi_curl_set[i].used) {
            set->multi_curl_set[i].used = 0;
            YB_APNs_LOG("set flag zero: %d(%p)",i,&(set->multi_curl_set[i]));
        }
    }
}

/*
 * multi curl 的 header 回调
 */
static size_t YB_connection_multi_http_header_callback(char *data,size_t size,size_t nitems,struct YBAPNsMulti_curl *multi_curl) {
    long sum = size * nitems;
    char *statusCode = YB_get_value_of_key(data, "HTTP/2", 3);
    if (statusCode) {
        YBAPNsRequestObject *req = YB_connection_getRequest(multi_curl->conn);
        YBAPNsRequestBody *bod = YB_request_getBody(req);
        char *pl =YB_request_getPayload(bod, kYBAPNsRequestPayloadAlert);
        YB_APNs_LOG("cmulti curl get response:%p:%d pl:%s",multi_curl->conn->curl,atoi(statusCode),pl);
        free(pl);
        multi_curl->used = 0;
        YB_count("c1");
        free(statusCode);
    }
    return sum;
}

static int observe2(CURLM *multi_handle,int *still_running) {
    int repeats = 0;
    int timeout = 0;
    YB_APNs_LOG("start observe,handle:%p",multi_handle);
    do {
        CURLMcode mc;
        int numfds = 0;
        mc = curl_multi_perform(multi_handle, still_running);
        if(mc == CURLM_OK ) {
            /* wait for activity, timeout or "nothing" */
            YB_APNs_LOG("++++++++++++-> wait for activity,timeout:25ms",NULL);
            mc = curl_multi_wait(multi_handle, NULL, 0, 25, &numfds);
        }
        if(mc != CURLM_OK) {
            YB_APNs_LOG("<-++++++++++++curl_multi failed, code %d.n",mc);
            return -1;
        }
        if(!numfds) {
            repeats++;
            YB_APNs_LOG("++++++++++++-> nothing to do: %d , still running: ####### %d #######",repeats,*still_running);
            if (repeats > 100) {
                YB_APNs_LOG("++++++++++++-> timeout,still running: ####### %d #######",*still_running);
                return 0;
            }
            
        }else {
            if (repeats == 0) {
                timeout++;
            }else
                repeats = 0;
            YB_APNs_LOG("++++++++++++-> activity found,timeout:%d",timeout);
            if (timeout > 500) {
                YB_APNs_LOG("++++++++++++-> activity timeout,return",NULL);
                return 0;
            }
        }
        
    }while (*still_running);
    return 1;
}


/*
 * multiplexing public api
 */

/*
 * 初始化一个set，len是apns.config 文件里的 multi-width 参数
 */
int YB_connection_multi_set_init(struct YBAPNsmulti_curl_set *set,size_t len,int max_conn) {
    long mc;
    set->multi_handle = curl_multi_init();
    mc = max_conn;
    // 设置多路复用
    curl_multi_setopt(set->multi_handle, CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);
    curl_multi_setopt(set->multi_handle, CURLMOPT_MAX_HOST_CONNECTIONS, mc);
    
    
    set->len = len;
    // 初始化每个multi_curl
    for (int i = 0; i < len; i ++) {
        if (!YB_connection_multi_curl_init(&(set->multi_curl_set[i]))) {
            return 0;
        }
        set->multi_curl_set[i].set = set;
    }
    return 1;
}

void YB_connection_multi_curl_set_cleaning_up(struct YBAPNsmulti_curl_set *set) {
    for (int i = 0; i < set->len; i++) {
        void *data = set->multi_curl_set[i].data;
        YBAPNsConnection *conn = set->multi_curl_set[i].conn;
        if (data){free(data);}
        if (conn) {YB_connection_clean(conn);}
    }
    if (set->multi_handle)
    {curl_multi_cleanup(set->multi_handle);}
}

/*
 * 初始化一个multi_curl
 */
int YB_connection_multi_curl_init(struct YBAPNsMulti_curl *mc) {
    if (mc->conn) {
        YB_connection_clean(mc->conn);
    }
    mc->conn = YB_connection_get_default_conn(1);
    if (mc->conn) {
        mc->data = NULL;
        mc->used = 0;
        mc->added = 0;
#if (CURLPIPE_MULTIPLEX > 0)
        /* wait for pipe connection to confirm */
        curl_easy_setopt(mc->conn->curl, CURLOPT_PIPEWAIT, 1L);
#endif
        curl_easy_setopt(mc->conn->curl, CURLOPT_HEADERDATA,mc);
        curl_easy_setopt(mc->conn->curl, CURLOPT_HEADERFUNCTION, YB_connection_multi_http_header_callback);
        //curl_easy_setopt(mc->conn->curl, CURLOPT_TIMEOUT, 20);
        return 1;
    }
    mc->used = 0;
    mc->added = 0;
    YB_APNs_DEBUG("multi_user_curl init error",NULL);
    return 0;
}

/*
 * multiplexing transform
 */
int YB_connection_multi_perform(struct YBAPNsmulti_curl_set *set) {
    YB_APNs_LOG("=====> beging perform a multi curl",NULL);
    int still_running = 0;
    CURLM *multi_handle = set->multi_handle;
    int count,obfd;
    // 添加 使用位 置1 的 curl
    count = 0;
    for (int i = 0; i < set->len; i++) {
        if (set->multi_curl_set[i].used == 1) {
            YB_APNs_LOG("add curl handle:%d(%p)",i,set->multi_curl_set[i].conn->curl);
            curl_multi_add_handle(multi_handle, set->multi_curl_set[i].conn->curl);
            set->multi_curl_set[i].added = 1;
            count++;
        }
    }
    if (count == 0) {
        YB_APNs_LOG("no curl to perform",NULL);
        return -1;
    }
    YB_APNs_LOG("--> perfom start",NULL);
    // 执行传输
    obfd = observe2(multi_handle, &still_running);
    remove_all_handle(set);
    if (obfd == 0) {
        YB_APNs_LOG("=====>connection was block,return",NULL);
        return 0;
    }
    set_used_zero(set);
    if (obfd == -1) {
        return -1;
    }
    return 1;
}





