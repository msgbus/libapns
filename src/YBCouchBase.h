//
//  YBCouchBase.h
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/1.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBCouchBase_h
#define YBCouchBase_h

#ifdef __cplusplus
extern "C"
{
#endif
#include <libcouchbase/couchbase.h>
#include <libcouchbase/api3.h>

#define COUCHBASE_HOST "couchbase://localhost/default"
#define COUCHBASE_PSW NULL

// couchbase 对象，用于连接couchbase
struct YB_couchbase_t {
    const char *cb;
    const char *psw;
    lcb_t instance;
    struct lcb_create_st create_opt;
};
// 初始化并连接couchbase。
extern int YB_couchbase_create(struct YB_couchbase_t *couchbase_t);
// 复用couchbase 链接，执行 取值操作
extern char * YB_couchbase_get_value(struct YB_couchbase_t *cb,const char *key);
// 对couchbase 链接执行清楚，一般不用
extern void YB_couchbase_cleaning_up(struct YB_couchbase_t *couchbase);

#ifdef __cplusplus
}
#endif

#endif /* YBCouchBase_h */
