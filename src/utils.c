/* vim:set ft=c ts=2 sw=2 sts=2 et cindent: */
/*
 * ***** BEGIN LICENSE BLOCK *****
 * Version: MIT
 *
 * Portions created by Alan Antonuk are Copyright (c) 2012-2013
 * Alan Antonuk. All Rights Reserved.
 *
 * Portions created by VMware are Copyright (c) 2007-2012 VMware, Inc.
 * All Rights Reserved.
 *
 * Portions created by Tony Garnock-Jones are Copyright (c) 2009-2010
 * VMware, Inc. and Tony Garnock-Jones. All Rights Reserved.
 *
 * Permission is hereby granted, free of charge, to any person
 * obtaining a copy of this software and associated documentation
 * files (the "Software"), to deal in the Software without
 * restriction, including without limitation the rights to use, copy,
 * modify, merge, publish, distribute, sublicense, and/or sell copies
 * of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be
 * included in all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND
 * NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS
 * BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN
 * ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN
 * CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 * ***** END LICENSE BLOCK *****
 */

#include <stdarg.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>

#include <stdint.h>
#include <amqp.h>
#include <amqp_framing.h>
#include <unistd.h>
#include <sys/stat.h>
#include <time.h>

#include "utils.h"
#include "cJSON.h"
#include "YBAPNsDebug.h"

#define MAX_COUNT_T 20
#define MAX_TIME_T 20

struct_conp *con_param = NULL;

static struct YB_count_t {
    const char *tag;
    uint64_t count;
} countStruct[MAX_COUNT_T];

static struct YB_time_t {
    const char *tag;
    time_t t;
}timeStruct[MAX_TIME_T];

void
config_file_parse() {
    FILE *fp;
    long file_size;
    char *tmp;
    // init con_param
    con_param = (struct_conp *)calloc(1, sizeof(struct_conp));
    if (!con_param) {
        die("malloc error: config struct malloc error");
    }
    
    // open file
    fp = fopen("apns.config", "rb");
    if (!fp) {
        die("lack apns-con config file");
    }
    fseek( fp , 0 , SEEK_END );
    file_size = ftell(fp);
    fseek( fp , 0 , SEEK_SET);
    
    tmp = (char *)calloc(1,file_size + 1);
    fread(tmp, file_size, 1, fp);
    fclose(fp);
    cJSON * json = cJSON_Parse(tmp);
    cJSON * mqHost = cJSON_GetObjectItem(json, "mq-host");
    con_param->mq_host = mqHost->valuestring ? strdup(mqHost->valuestring): NULL;
    cJSON * mqPort = cJSON_GetObjectItem(json, "mq-port");
    con_param->mq_port = mqPort->valueint;
    cJSON * mqEx = cJSON_GetObjectItem(json, "mq-exchange");
    con_param->mq_exchange = mqEx->valuestring ? strdup(mqEx->valuestring) : NULL;
    cJSON * mqRo = cJSON_GetObjectItem(json, "mq-routingKey");
    con_param->mq_routingKey = mqRo->valuestring ? strdup(mqRo->valuestring) : NULL;
    cJSON * mqQueue = cJSON_GetObjectItem(json, "mq-queue");
    con_param->mq_queue = mqQueue->valuestring ? strdup(mqQueue->valuestring) : NULL;
    cJSON * couchbase = cJSON_GetObjectItem(json, "couchbase");
    con_param->couchbase = couchbase->valuestring ? strdup(couchbase->valuestring) : NULL;
    cJSON * isMulti = cJSON_GetObjectItem(json, "is-multi");
    con_param->isMulti = isMulti->valueint;
    cJSON * cert_timeout = cJSON_GetObjectItem(json, "cert-timeout");
    con_param->cert_timeout = cert_timeout->valueint;
    cJSON * multi_Width = cJSON_GetObjectItem(json, "multi-width");
    con_param->multi_Width = multi_Width->valueint;
    cJSON * max_conn = cJSON_GetObjectItem(json, "max-conn");
    con_param->max_conn = max_conn->valueint;
    cJSON_Delete(json);
}

void confrim_file_parse() {
    // confirm server and port
    if (!con_param->mq_host || con_param->mq_port == 0) {
        die("server config param error");
    }
    // confirm mq exchang/routingKey/queueStr
    if ((!con_param->mq_routingKey || !con_param->mq_exchange) || !con_param->mq_queue) {
        die("incorrect queue,exchange,routingKey config param");
    }
    // confirm runMode
    if (con_param->isMulti != 0 && con_param->isMulti != 1) {
        die("incorrect run-mode in config file");
    }
    // confirm multiWidth under multi mode
    if ((con_param->multi_Width <= 0 || con_param->multi_Width > MAX_MULTI_CURL_SET)
        && con_param->isMulti == 1) {
        die("multiWidth in config file must > 0 and <= %d",MAX_MULTI_CURL_SET);
    }
    // confirm couchbase
    if (!con_param->couchbase) {
        die("couchbase config param error");
    }
    // confirm cert_observer_timeout
    if (con_param->cert_timeout <= 0 || con_param->cert_timeout > 300) {
        die("cert_observer timeout must > 0 & <300 sec");
    }
}

void slog(const char *fmt, ...)
{
    if (1) {
        va_list ap;
        va_start(ap, fmt);
        vfprintf(stderr, fmt, ap);
        va_end(ap);
        fprintf(stderr,"\n");
    }
}

void
YB_count_start(const char *tag) {
    int  i = 0;
    while (i < MAX_COUNT_T) {
        if (countStruct[i].tag && strcmp(tag, countStruct[i].tag) == 0) {
            countStruct[i].count = 0;
            slog("===> count << %s : 0 >>",tag);
            return;
        }
        i++;
    }
    i = 0;
    while (!countStruct[i].tag) {
        countStruct[i].count = 0;
        countStruct[i].tag = strdup(tag);
        slog("===> count << %s : 0 >>",tag);
        return;
    }
    slog("===> count struct is full");
    return;
}

void
YB_count(const char *tag) {
    int i = 0;
    while (countStruct[i].tag) {
        if (strcmp(tag, countStruct[i].tag) == 0) {
            slog("===> count << %s : %lld >>",tag,++countStruct[i].count);
        }
        i++;
    }
}

void
YB_time_start(const char *tag) {
    int  i = 0;
    while (i < MAX_TIME_T) {
        if (timeStruct[i].tag && strcmp(tag, timeStruct[i].tag) == 0) {
            timeStruct[i].t = time(NULL);
            slog("===> time << %s : 0 >>",tag);
            return;
        }
        i++;
    }
    i = 0;
    while (i < MAX_TIME_T) {
        if (!timeStruct[i].tag) {
            timeStruct[i].t = time(NULL);
            timeStruct[i].tag = strdup(tag);
            slog("===> time << %s : 0 >>",tag);
            return;
        }
        i++;
    }
    slog("===> time struct is full");
    return;
}

void
YB_time(const char *tag) {
    int i = 0;
    while (timeStruct[i].tag) {
        if (strcmp(tag, timeStruct[i].tag) == 0) {
            slog("===> time << %s : %ld >>",tag,time(NULL) - timeStruct[i].t);
            return;
        }
        i++;
    }
}
void
YB_time_str(const char *tag,const char *str) {
    int i = 0;
    while (timeStruct[i].tag) {
        if (strcmp(tag, timeStruct[i].tag) == 0) {
            slog("===> time << %s : %ld >> : %s",tag,time(NULL) - timeStruct[i].t,str);
            return;
        }
        i++;
    }
}

void die(const char *fmt, ...)
{
  va_list ap;
  va_start(ap, fmt);
  vfprintf(stderr, fmt, ap);
  va_end(ap);
  fprintf(stderr, "\n");
  exit(1);
}

void die_on_error(int x, char const *context)
{
  if (x < 0) {
    fprintf(stderr, "%s: %s\n", context, amqp_error_string2(x));
    exit(1);
  }
}

void die_on_amqp_error(amqp_rpc_reply_t x, char const *context)
{
  switch (x.reply_type) {
  case AMQP_RESPONSE_NORMAL:
    return;

  case AMQP_RESPONSE_NONE:
    fprintf(stderr, "%s: missing RPC reply type!\n", context);
    break;

  case AMQP_RESPONSE_LIBRARY_EXCEPTION:
    fprintf(stderr, "%s: %s\n", context, amqp_error_string2(x.library_error));
    break;

  case AMQP_RESPONSE_SERVER_EXCEPTION:
    switch (x.reply.id) {
    case AMQP_CONNECTION_CLOSE_METHOD: {
      amqp_connection_close_t *m = (amqp_connection_close_t *) x.reply.decoded;
      fprintf(stderr, "%s: server connection error %uh, message: %.*s\n",
              context,
              m->reply_code,
              (int) m->reply_text.len, (char *) m->reply_text.bytes);
      break;
    }
    case AMQP_CHANNEL_CLOSE_METHOD: {
      amqp_channel_close_t *m = (amqp_channel_close_t *) x.reply.decoded;
      fprintf(stderr, "%s: server channel error %uh, message: %.*s\n",
              context,
              m->reply_code,
              (int) m->reply_text.len, (char *) m->reply_text.bytes);
      break;
    }
    default:
      fprintf(stderr, "%s: unknown server error, method id 0x%08X\n", context, x.reply.id);
      break;
    }
    break;
  }

  exit(1);
}






