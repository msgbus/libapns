//
//  YBAPNs.h
//  YBApns2
//
//  Created by 云巴pro on 16/8/16.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#ifndef YBAPNs_h
#define YBAPNs_h


#ifdef __cplusplus
extern "C" {
#endif

#include <stdio.h>
#include <curl/curl.h>
#include "utils.h"
#include "YBAPNsRequest.h"
#include "YBAPNsResponse.h"

/*
 * 默认的服务器ip地址和默认的子路径，可以通过修改此值重新设置默认路径
 */
static const char *kYBDefaultAPNsServerAddr = "https://api.development.push.apple.com:443";
static const char *kYBDefaultAPNsServerSubPath = "/3/device";

#define YB_APNS_PUSH_DEV_ADDR "https://api.development.push.apple.com:443"
#define YB_APNS_PUSH_PRD_ADDR "https://api.push.apple.com:443"
    
typedef enum YBAPNsConfig {
    YBAPNsConfigServer,
    YBAPNsConfigSubPath,
}YBAPNsConfig;

extern const char * const YBAPNsConfigStr[]; /* use YBAPNsConfigStr[YBAPNsConfigXXX] to search string for enum YBAPNsConfig */

extern struct YBAPNs_config YBAPNs_config; /* configuration list object */


typedef struct YBAPNsConncetion YBAPNsConnection; /* core object: connection object */
    
struct YBAPNsMulti_curl {
    YBAPNsConnection *conn;
    struct YBAPNsmulti_curl_set *set;
    void *data;
    int used;
    int added;
};
    
struct YBAPNsmulti_curl_set {
    struct YBAPNsMulti_curl multi_curl_set[MAX_MULTI_CURL_SET];
    CURLM * multi_handle;
    size_t len;
};
    
//extern void YB_connection_set_FILE(YBAPNsConnection *connection,FILE *fp);

//extern FILE * YB_connection_get_FILE(const YBAPNsConnection *conn);


/*
 * config function for the config struct
 */
extern void YB_APNs_SetConfig(const char * config,YBAPNsConfig key);
extern const char * YB_APNs_getConfig(YBAPNsConfig key);
/*
 * 全局设定，该函数在一个application中应该只调用一次。
 */
extern void YB_connection_global_init();

/*
 * 该函数用以便利地创建并返回一个使用默认值的connection对象，注意默认值是不可用的
 */
extern YBAPNsConnection * YB_connection_get_default_conn();

/*
 * init func for YBAPNsConnection
 */
extern YBAPNsConnection * YB_connection_init(YBAPNsRequestObject * request);
/*
 * init func for YBAPNsConnection,this function will clean all object : connection request response and curl
 */
extern void YB_connection_clean(YBAPNsConnection * connection);

/*
 * update connection object with request,you can use it to update data like header or payload
 */
extern int YB_connection_setHeader(YBAPNsConnection *connection,YBAPNsRequestObject * request);

extern void YB_connection_setVerbose(YBAPNsConnection *connection,int verbose);

/*
 * getMethod
 */
extern YBAPNsRequestObject * YB_connection_getRequest(YBAPNsConnection * connection);

// setup curl object in connection object(如果要使用非多路复用推送，可以直接调用YB_connection_perform，无需调用此接口)
extern int YB_connection_curl_prepare(YBAPNsConnection * connection);
    
/*
 * perform a http2 transfer to APNs,it will return a responseObject
 */
extern YBAPNsResponseObject * YB_connection_perform(YBAPNsConnection * conncetion);
    
    
/*
 * multi API
 */
// init function for YBAPNsmulti_curl_set
extern int YB_connection_multi_set_init(struct YBAPNsmulti_curl_set *set,size_t len,int max_conn);
    
extern void YB_connection_multi_curl_set_cleaning_up(struct YBAPNsmulti_curl_set *set);
/*
 * init function for YBAPNsMulti_curl,you should create a YBAPNsmulti_curl_set and call
 * YB_connection_multi_set_init function,and should not call this function directly
 */
extern int YB_connection_multi_curl_init(struct YBAPNsMulti_curl *mc);
    
/*
 * perform a multiplexing transfer
 * you should call this function after 
 * YBAPNsmulti_curl_set object and YBAPNsMulti_curl object inside was prepare
 */
extern int YB_connection_multi_perform(struct YBAPNsmulti_curl_set *set);

#ifdef __cplusplus
}
#endif

#endif /* YBAPNs_h */
