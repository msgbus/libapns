//
//  test_requestset.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/25.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"


#if 0
int main()
{
    YBAPNsRequestHeader * header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "token", "apnsId", 10, kYBReqPriorityHigh, "topic");
    YBAPNsRequestBody * body = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject * object = YB_request_object_init(header, body, "pem", "path", "123");
    
    YBAPNsRequestHeader *newheader = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "token1", "apnsId1", 10, kYBReqPriorityHigh, "topic1");
    YBAPNsRequestBody * newbody = YB_request_body_init("alert1", 1, "default", NULL);
    
    printf("========> YB_request_setHeader\n");
    printf("====> test NULL\n");
    printf("object null %d\n",YB_request_setHeader(NULL, newheader));
    printf("header null %d\n",YB_request_setHeader(object, NULL));
    printf("====> test new header\n");
    printf("new header:%d\n",YB_request_setHeader(object, newheader));
    
    printf("========> YB_request_setBody\n");
    printf("====> test NULL\n");
    printf("object null %d\n",YB_request_setBody(NULL, newbody));
    printf("body null %d\n",YB_request_setBody(object, NULL));
    printf("====> test new body\n");
    printf("new header:%d\n",YB_request_setBody(object, newbody));
    
    printf("========> YB_request_setCer\n");
    printf("====> test null\n");
    printf("object null %d\n",YB_request_setCer(NULL, "PEM", "PATH", "123"));
    printf("type null %d\n",YB_request_setCer(object, NULL, "PATH", "123"));
    printf("path null %d\n",YB_request_setCer(object, "PEM", NULL, "123"));
    printf("psw null %d\n",YB_request_setCer(object, "PEM", "PATH", NULL));
    printf("all null %d\n",YB_request_setCer(NULL, NULL, NULL, NULL));
    printf("all null %d\n",YB_request_setCer(object, "PEM", "PATH", "123"));
}
#endif
