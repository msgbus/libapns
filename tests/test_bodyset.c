//
//  test_bodyset.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/25.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>

#include "YBAPNs.h"

#if 0
int main()
{
    YBAPNsRequestBody *body = YB_request_body_init("alert", 0, NULL, NULL);
    
    // body == NULL
    printf("=====>test body == NULL\n");
    printf("Alert:%d\n",YB_request_setPayload(NULL, "", 0, kYBAPNsRequestPayloadAlert));
    printf("Badge:%d\n",YB_request_setPayload(NULL, "", 0, kYBAPNsRequestPayloadBadge));
    printf("Sound:%d\n",YB_request_setPayload(NULL, "", 0, kYBAPNsRequestPayloadSound));
    printf("BodyStr:%d\n",YB_request_setPayload(NULL, "", 0, kYBAPNsRequestPayloadBodyStr));
    printf("badge integer non zero:%d\n",YB_request_setPayload(NULL, "", 100, kYBAPNsRequestPayloadBodyStr));
    printf("key overflow:%d\n",YB_request_setPayload(NULL, "", 0, 10));
    
    // test strPayload param
    printf("=====>test strPayload parameters\n");
    printf("Alert:%d\n",YB_request_setPayload(body, "content", 0, kYBAPNsRequestPayloadAlert));
    printf("Alert(NULL):%d\n",YB_request_setPayload(body, NULL, 0, kYBAPNsRequestPayloadAlert));
    printf("Sound:%d\n",YB_request_setPayload(body, "content", 0, kYBAPNsRequestPayloadSound));
    printf("Sound(NULL):%d\n",YB_request_setPayload(body, NULL, 0, kYBAPNsRequestPayloadSound));
    printf("bodyStr:%d\n",YB_request_setPayload(body, "content", 0, kYBAPNsRequestPayloadBodyStr));
    printf("bodyStr(NULL):%d\n",YB_request_setPayload(body, NULL, 0, kYBAPNsRequestPayloadBodyStr));
    printf("key overflow:%d\n",YB_request_setPayload(body, "content", 0, 10));
    printf("key overflow(NULL):%d\n",YB_request_setPayload(body, NULL, 0, 10));
    
    // test badge uint32_t
    printf("=====>test badge uint32_t\n");
    printf("<0:%d\n",YB_request_setPayload(body, NULL , -1, kYBAPNsRequestPayloadBadge));
    printf("=0:%d\n",YB_request_setPayload(body, NULL , 0, kYBAPNsRequestPayloadBadge));
    printf(">0:%d\n",YB_request_setPayload(body, NULL , 100, kYBAPNsRequestPayloadBadge));
    
    // test key
    printf("=====>test key\n");
    printf("key <0:%d\n",YB_request_setPayload(body, NULL , -1, -10));
    printf("key >3:%d\n",YB_request_setPayload(body, NULL , -1, 5));
    
    
}
#endif
