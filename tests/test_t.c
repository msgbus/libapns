//
//  test_t.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/9/12.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include <curl/curl.h>

#if 0
// /3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953b
int main() {
    CURL * curl = curl_easy_init();
    
    if(curl){
        //curl_easy_setopt(curl, CURLOPT_TIMEOUT , 10);设置超时
        
        curl_easy_setopt(curl, CURLOPT_VERBOSE , 1L);                       //打印调试信息
        struct curl_slist * http_headers;
        http_headers = curl_slist_append(http_headers, "apns-expiration:1");
        http_headers = curl_slist_append(http_headers, "apns-priority:10");
        http_headers = curl_slist_append(http_headers, "apns-topic:com.yunba.apnsnewCAI.apnsCAI2");
        
        curl_easy_setopt(curl,CURLOPT_POST,1L);                              //设置为非0表示本次操作为post
        curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);//HTTP/2协议
        curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_headers);           //添加 httpheader
        
        curl_easy_setopt(curl, CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953b");
        curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
        curl_easy_setopt(curl, CURLOPT_SSLCERT, "/Users/yunbapro/Desktop/apns.pem");
        curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"{ \"aps\": { \"alert\" : \"hello\" } }");//携带json参数进行post
        
        
        CURLcode response = curl_easy_perform(curl);
        
        fprintf(stderr, "curl_easy_perform() failed: %s\n",
                curl_easy_strerror(response));
        
        curl_slist_free_all(http_headers);
        curl_easy_cleanup(curl);
    }

    return 0;
}

#endif