//
//  test_headerset.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/25.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"


#if 0
int main()
{
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "d", NULL , 0, kYBReqPriorityHigh, NULL);
    
    printf("=============> YB_request_setAttribute()\n");
    // test header = NULL
    printf("======> test header == NULL\n");
    printf("apnsTopic:%d\n",YB_request_setAttribute(NULL, "topic", 0, kYBAPNsRequestAttributeApnsTopic));
    printf("token:%d\n",YB_request_setAttribute(NULL, "token", 0, kYBAPNsRequestAttributeDeviceToken));
    printf("expiration:%d\n",YB_request_setAttribute(NULL, "exp", 0, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsId:%d\n",YB_request_setAttribute(NULL, "apnsId", 0, kYBAPNsRequestAttributeApnsId));
    
    // test Attribute
    printf("======> test attribute\n");
    printf("apnsTopic:%d\n",YB_request_setAttribute(header, "topic", 0, kYBAPNsRequestAttributeApnsTopic));
    printf("apnsTopic(NULL):%d\n",YB_request_setAttribute(header, NULL, 0, kYBAPNsRequestAttributeApnsTopic));
    printf("deviceToken:%d\n",YB_request_setAttribute(header, "token", 0, kYBAPNsRequestAttributeDeviceToken));
    printf("deviceToken(NULL):%d\n",YB_request_setAttribute(header, NULL, 0, kYBAPNsRequestAttributeDeviceToken));
    printf("expiration:%d\n",YB_request_setAttribute(header, NULL, 20, kYBAPNsRequestAttributeApnsExpiration));
    printf("expiration(max):%d\n",YB_request_setAttribute(header, NULL, MAXIMUN_APNS_EXPIRATION_SIZE, kYBAPNsRequestAttributeApnsExpiration));
    printf("expiration(min):%d\n",YB_request_setAttribute(header, NULL, MINIMUM_APNS_EXPIRATION_SIZE, kYBAPNsRequestAttributeApnsExpiration));
    printf("expiration(max+1):%d\n",YB_request_setAttribute(header, NULL, MAXIMUN_APNS_EXPIRATION_SIZE + 1, kYBAPNsRequestAttributeApnsExpiration));
    printf("expiration(min-1):%d\n",YB_request_setAttribute(header, NULL, MINIMUM_APNS_EXPIRATION_SIZE - 1, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsId:%d\n",YB_request_setAttribute(header, "apnsId", 0, kYBAPNsRequestAttributeApnsId));
    printf("apnsId(NULL):%d\n",YB_request_setAttribute(header, NULL, 0, kYBAPNsRequestAttributeApnsId));
    
    // test key
    printf("======> test key\n");
    printf("<0:%d\n",YB_request_setAttribute(header, "d", 0, -4));
    printf(">=4:%d\n",YB_request_setAttribute(header, "d", 0, 4));
    
    
    printf("=============> YB_request_setPriority()\n");
    // test NULL
    printf("======> test NULL\n");
    printf("priorityHigh:%d\n",YB_request_setPriority(NULL,kYBReqPriorityHigh));
    printf("priorityLow:%d\n",YB_request_setPriority(NULL,kYBReqPriorityLow));
    // test key
    printf("======> test key");
    printf("priorityHigh:%d\n",YB_request_setPriority(header,kYBReqPriorityHigh));
    printf("priorityLow:%d\n",YB_request_setPriority(header,kYBReqPriorityLow));
    printf("priority < 0:%d\n",YB_request_setPriority(header,-10));
    printf("priority != 10 !=5:%d\n",YB_request_setPriority(header,20));
    
    printf("=============> YB_request_setMethod()\n");
    printf("======> test NULL\n");
    printf("POST:%d\n",YB_request_setMethod(NULL,kYBAPNsRequestMethodTypePOST));
    printf("======> test key\n");
    printf("POST:%d\n",YB_request_setMethod(header,kYBAPNsRequestMethodTypePOST));
    printf("< 0:%d\n",YB_request_setMethod(header,-10));
    printf("> 0:%d\n",YB_request_setMethod(header,10));
    return 0;
}

#endif
