//
//  test_init.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "YBAPNs.h"


#if 0
int main() {
    printf("==========> test YB_request_header_init\n");
    printf("====> test method key\n");
    printf("|method key < 0|\n");
    YB_request_header_init(-1, "deviceToken", "apnsId", 100, kYBReqPriorityHigh, "topic");
    printf("|method key > 1|\n");
    YB_request_header_init(2, "deviceToken", "apnsId", 100, kYBReqPriorityHigh, "topic");
    printf("|method key = POST|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "deviceToken", "apnsId", 100, kYBReqPriorityHigh, "topic");
    
    printf("====> test deviceToken key\n");
    printf("|deviceToken NULL|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, NULL, "apnsId", 100, kYBReqPriorityHigh, "topic");
    printf("|deviceToken not NULL|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, kYBReqPriorityHigh, "topic");
    
    printf("====> test expiration key\n");
    printf("|expiraiton|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, kYBReqPriorityHigh, "topic");
    
    printf("====> test priority key\n");
    printf("|priority high|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "deviceToken", "apnsId", 100, kYBReqPriorityHigh, "topic");
    printf("|priority low|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, kYBReqPriorityLow, "topic");
    printf("|priority <0|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, -1, "topic");
    printf("|priority >1|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, 2, "topic");
    
    printf("====> test topic\n");
    printf("|topic NULL|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, kYBReqPriorityHigh, NULL);
    printf("|topic not NULL|\n");
    YB_request_header_init(kYBAPNsRequestMethodTypePOST, "xxx-ccc", "apnsId", 100, kYBReqPriorityHigh, "topic");
    
    
    printf("==========> test YB_request_body_init\n");
    printf("====> test Alert key\n");
    printf("|alert NULL|\n");
    YB_request_body_init(NULL, 1, "default", NULL);
    printf("|alert not NULL|\n");
    YB_request_body_init("alert", 1, "default", NULL);
    printf("====> test badge key\n");
    printf("|badge 0|\n");
    YB_request_body_init("alert", 0, "default", NULL);
    printf("|badge < 0|\n");
    YB_request_body_init("alert", -1, "default", NULL);
    printf("|bage > 0|\n");
    YB_request_body_init("alert", 2, "default", NULL);
    printf("====> test soudn key\n");
    printf("|sound NULL|\n");
    YB_request_body_init("alert", 1, NULL, NULL);
    printf("|sound not NULL|\n");
    YB_request_body_init("alert", 1, "default", NULL);
    printf("====> test bodyStr key\n");
    printf("|bodyStr NULL|\n");
    YB_request_body_init("alert", 1, "default", NULL);
    printf("|bodyStr not NULL|\n");
    YB_request_body_init("alert", 1, "default", "{alert:s,sound:d,badge:1}");
    
    printf("==========> test YB_request_object_init\n");
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body = YB_request_body_init("alert", 1, "default", NULL);
    printf("====> header NULL\n");
    YB_request_object_init(NULL, body, "pem", "aa/bb/cc", "123");
    printf("====> body NULL\n");
    YB_request_object_init(header, NULL, "pem", "aa/bb/cc", "123");
    printf("====> cerType NULL\n");
    YB_request_object_init(header, body, NULL, "aa/bb/cc", "123");
    printf("====> cerPath NULL\n");
    YB_request_object_init(header, body, "pem",NULL, "123");
    printf("====> cerPsw NULL\n");
    YB_request_object_init(header, body, "pem", "aa/bb/cc", NULL);
    printf("====> not NULL\n");
    YB_request_object_init(header, body, "pem", "aa/bb/cc", "123");
    
    printf("==========> test YB_connection_init\n");
    YBAPNsRequestHeader *header1 = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body1 = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object1 = YB_request_object_init(header1, body1, "pem", "aa/bb/cc", "123");
    printf("====> test NULL\n");
    YB_connection_init(NULL);
    printf("====> test not NULL\n");
    YB_connection_init(object1);
    
    
    return 0;
}
#endif