//
//  test_perform.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"

#if 0
int main()
{
    printf("%s",curl_version());
    YB_connection_global_init();
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953c", NULL, 100, kYBReqPriorityHigh, "com.yunba.apnsnewCAI.apnsCAI2");
    YBAPNsRequestBody *body = YB_request_body_init(NULL, 0, NULL, "{ \"aps\": { \"alert\" : \"NEW NEW\" } }");
    YBAPNsRequestObject *request = YB_request_object_init(header, body, "PEM", "/Users/yunbapro/Desktop/apns.pem",NULL);
    YBAPNsConnection *connection = YB_connection_init(request);
    printf("==> set verbose\n");
    YB_connection_setVerbose(connection, 1);
    printf("<== set verbose success\n\n");
    YBAPNsResponseObject *response = YB_connection_perform(connection);
    if (response) {
        printf("==> get a response\n");
        printf("statusCode : %d\n",response->statusCode);
        printf("apnsId : %s\n",response->apnsId);
        printf("reason : %s\n",YBAPNsResReasonStr[response->reason]);
        printf("timestamp : %s\n",response->timestamp);
    }
    return 0;
}
#endif
