//
//  test_clean.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "YBAPNs.h"

#if 0
int main()
{
    printf("==========> test connection clean\n");
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object = YB_request_object_init(header, body, "pem", "aa/bb/cc", "123");
    YBAPNsConnection *connection = YB_connection_init(object);
    printf("-- param is NULL\n"); YB_connection_clean(NULL);
    printf("-- param is not NULL\n"); YB_connection_clean(connection);
    
    printf("==========> test request clean\n");
    YBAPNsRequestHeader *header1 = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body1 = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object1 = YB_request_object_init(header1, body1, "pem", "aa/bb/cc", "123");
    printf("-- param is NULL\n"); YB_request_object_clean(NULL);
    printf("-- param is not NULL\n"); YB_request_object_clean(object1);
    
    printf("==========> test header clean\n");
    YBAPNsRequestHeader *header2 = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    printf("-- param is NULL\n"); YB_request_header_clean(NULL);
    printf("-- param is not NULL\n"); YB_request_header_clean(header2);
    
    printf("==========> test body clean\n");
    YBAPNsRequestBody *body2 = YB_request_body_init("alert", 1, "default", NULL);
    printf("-- param is NULL\n"); YB_request_body_clean(NULL);
    printf("-- param is not NULL\n"); YB_request_body_clean(body2);
    
    printf("==========> test response clean\n");
    YBAPNsResponseObject *response = (YBAPNsResponseObject *)calloc(1,sizeof(YBAPNsResponseObject));
    response->apnsId = strdup("xx-dd-ww");
    response->reason = kYBResReasonBadDevToken;
    response->statusCode = kYBAPNsResCodeBadRequest;
    response->timestamp = strdup("abc");
    printf("-- param is NULL\n"); YB_response_clean(NULL);
    printf("-- param is NULL\n"); YB_response_clean(response);
    
    return 0;
}
#endif
