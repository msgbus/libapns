//
//  test_bodyget.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/25.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"

#if 0
int main()
{
    YBAPNsRequestBody *body = YB_request_body_init("alert", 1, "default", NULL);
    
    printf("==========> test YB_request_getPayload\n");
    printf("====> test NULL\n");
    printf("alert:%s\n",YB_request_getPayload(NULL, kYBAPNsRequestPayloadAlert));
    printf("sound:%s\n",YB_request_getPayload(NULL, kYBAPNsRequestPayloadSound));
    printf("alert:%s\n",YB_request_getPayload(NULL, kYBAPNsRequestPayloadBodyStr));
    printf("alert:%s\n",YB_request_getPayload(NULL, kYBAPNsRequestPayloadBadge));
    
    printf("====> test key\n");
    printf("alert:%s\n",YB_request_getPayload(body, kYBAPNsRequestPayloadAlert));
    printf("sound:%s\n",YB_request_getPayload(body, kYBAPNsRequestPayloadSound));
    printf("badge:%s\n",YB_request_getPayload(body, kYBAPNsRequestPayloadBadge));
    printf("bodyStr:%s\n",YB_request_getPayload(body, kYBAPNsRequestPayloadBodyStr));
    printf("key > 3:%s\n",YB_request_getPayload(body, 20));
    printf("key < 0:%s\n",YB_request_getPayload(body, -10));
    
    printf("==========> test YB_request_getIntPayload\n");
    printf("====> test NULL\n");
    printf("alert:%u\n",YB_request_getIntPayload(NULL, kYBAPNsRequestPayloadAlert));
    printf("sound:%u\n",YB_request_getIntPayload(NULL, kYBAPNsRequestPayloadSound));
    printf("bodyStr:%u\n",YB_request_getIntPayload(NULL, kYBAPNsRequestPayloadBodyStr));
    printf("alert:%u\n",YB_request_getIntPayload(NULL, kYBAPNsRequestPayloadBadge));
    
    printf("====> test key\n");
    printf("alert:%u\n",YB_request_getIntPayload(body, kYBAPNsRequestPayloadAlert));
    printf("sound:%u\n",YB_request_getIntPayload(body, kYBAPNsRequestPayloadSound));
    printf("badge:%u\n",YB_request_getIntPayload(body, kYBAPNsRequestPayloadBadge));
    printf("bodyStr:%u\n",YB_request_getIntPayload(body, kYBAPNsRequestPayloadBodyStr));
    printf("key > 3:%u\n",YB_request_getIntPayload(body, 20));
    printf("key < 0:%u\n",YB_request_getIntPayload(body, -10));
}

#endif