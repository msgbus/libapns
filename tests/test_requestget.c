//
//  test_requestget.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"

#if 0
int main()
{
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object = YB_request_object_init(header, body, "pem", "aa/bb/cc", "123");
    YBAPNsConnection *connection = YB_connection_init(object);
    
    YBAPNsRequestHeader *header1 = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body1 = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *objectNotAdded = YB_request_object_init(header1, body1, "pem", "aa/bb/cc", "123");
    printf("==========> test YB_request_getHeader\n");
    printf("====> test NULL\n");
    printf("header:%p\n",YB_request_getHeader(NULL));
    printf("====> test not NULL\n");
    printf("header:%p\n",YB_request_getHeader(object));
    
    printf("==========> test YB_request_getBody\n");
    printf("====> test NULL\n");
    printf("body:%p\n",YB_request_getBody(NULL));
    printf("====> test not NULL\n");
    printf("body:%p\n",YB_request_getBody(object));
    
    printf("==========> test YB_request_getFlag\n");
    printf("====> test NULL\n");
    printf("flag:%d\n",YB_request_getFlag(NULL));
    printf("====> test Not NULL\n");
    printf("not add to a connection flag:%d\n",YB_request_getFlag(objectNotAdded));
    printf("added to a connection flag:%d\n",YB_request_getFlag(object));
    
    printf("==========> test YB_request_getCer\n");
    printf("====> test NULL\n");
    printf("cerType:%s\n",YB_request_getCer(NULL, kYBAPNsRequestCerType));
    printf("cerPath:%s\n",YB_request_getCer(NULL, kYBAPNsRequestCerPath));
    printf("cerPsw:%s\n",YB_request_getCer(NULL, kYBAPNsRequestCerPsw));
    printf("====> test key\n");
    printf("cerType:%s\n",YB_request_getCer(object, kYBAPNsRequestCerType));
    printf("cerPath:%s\n",YB_request_getCer(object, kYBAPNsRequestCerPath));
    printf("cerPsw:%s\n",YB_request_getCer(object, kYBAPNsRequestCerPsw));
    printf("key > 2:%s\n",YB_request_getCer(object, 3));
    printf("key < 0:%s\n",YB_request_getCer(object, -1));
    return 0;
}
#endif
