//
//  main.c
//  testMutiCurl
//
//  Created by 蔡弘基 on 16/9/6.
//  Copyright © 2016年 蔡弘基. All rights reserved.
//

#include <stdio.h>
#include <curl/curl.h>
#include <unistd.h>
#include <sys/time.h>
#if 0

static void setCurl(CURL *curl) {
    curl_easy_setopt(curl, CURLOPT_VERBOSE , 1L);
    struct curl_slist * http_headers = NULL;
    http_headers = curl_slist_append(http_headers, "apns-expiration:1");
    http_headers = curl_slist_append(http_headers, "apns-priority:10");
    http_headers = curl_slist_append(http_headers, "com.yunba.apnsnewCAI.apnsCAI2");
    
    curl_easy_setopt(curl,CURLOPT_POST,1);
    curl_easy_setopt(curl, CURLOPT_HTTP_VERSION, CURL_HTTP_VERSION_2_0);
    curl_easy_setopt(curl, CURLOPT_HTTPHEADER, http_headers);
    
    curl_easy_setopt(curl, CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953c");
    curl_easy_setopt(curl, CURLOPT_SSLCERTTYPE, "PEM");
    curl_easy_setopt(curl, CURLOPT_SSLCERT, "/Users/yunbapro/Desktop/apns.pem");
    curl_easy_setopt(curl, CURLOPT_POSTFIELDS,"{ \"aps\": { \"alert\" : \"hello\" } }");
    
    curl_easy_setopt(curl, CURLOPT_PIPEWAIT, 1L);
}

int main(int argc, const char * argv[]) {
    CURL *easy[4];
    CURLM *multi_handle;
    int still_running;
    CURLMcode mc;
    
    multi_handle = curl_multi_init();
    
    for (int i = 0; i <4; i++) {
        easy[i] = curl_easy_init();
        setCurl(easy[i]);
        switch (i) {
            case 0:
                curl_easy_setopt(easy[i], CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953c");
                break;
            case 1:
                curl_easy_setopt(easy[i], CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953b");
                break;
            case 2:
                curl_easy_setopt(easy[i], CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953c");
                break;
                case 3:
                curl_easy_setopt(easy[i], CURLOPT_URL,"https://api.development.push.apple.com:443/3/device/54db968d17f3c1bfd4457789deb5a77c1cc8a29e3b7ba3a361fe24bd5433953c");
                break;
                
            default:
                break;
        }
        curl_multi_add_handle(multi_handle, easy[i]);
    }
    
    curl_multi_setopt(multi_handle, CURLMOPT_PIPELINING, CURLPIPE_MULTIPLEX);
    
    curl_multi_setopt(multi_handle, CURLMOPT_MAX_HOST_CONNECTIONS, 1L);
    
    mc = curl_multi_perform(multi_handle, &still_running);
    if (mc != CURLM_OK) {
        printf("failed");
        return 0;
    }
    printf("############ start %d ##############\n",still_running);
    do {
        struct timeval timeout;
        int rc; /* select() return code */
        CURLMcode mc; /* curl_multi_fdset() return code */
        
        fd_set fdread;
        fd_set fdwrite;
        fd_set fdexcep;
        int maxfd = -1;
        
        long curl_timeo = -1;
        
        FD_ZERO(&fdread);
        FD_ZERO(&fdwrite);
        FD_ZERO(&fdexcep);
        
        /* set a suitable timeout to play around with */
        timeout.tv_sec = 1;
        timeout.tv_usec = 0;
        curl_multi_timeout(multi_handle, &curl_timeo);
        if(curl_timeo >= 0) {
            timeout.tv_sec = curl_timeo / 1000;
            if(timeout.tv_sec > 1)
                timeout.tv_sec = 1;
            else
                timeout.tv_usec = (curl_timeo % 1000) * 1000;
        }
        /* get file descriptors from the transfers */
        printf("############ fd set ##############\n");
        mc = curl_multi_fdset(multi_handle, &fdread, &fdwrite, &fdexcep, &maxfd);
        if(mc != CURLM_OK) {
            fprintf(stderr, "curl_multi_fdset() failed, code %d.\n", mc);
            break;
        }
        if(maxfd == -1) {
            /* Portable sleep for platforms other than Windows. */
            struct timeval wait = { 0, 100 * 1000 }; /* 100ms */
            printf("############ select -1 start ##############\n");
            rc = select(0, NULL, NULL, NULL, &wait);
            printf("############ select -1 end ##############\n");
        }
        else {
            /* Note that on some platforms 'timeout' may be modified by select().
             If you need access to the original value save a copy beforehand. */
            printf("############ select %d start ##############\n",maxfd);
            rc = select(maxfd+1, &fdread, &fdwrite, &fdexcep, &timeout);
            printf("############ select %d end ##############\n",maxfd);
        }
        switch(rc) {
            case -1:
                /* select error */
                break;
            case 0:
            default:
                /* timeout or readable/writable sockets */
                printf("############ perform %d ##############\n",still_running);
                curl_multi_perform(multi_handle, &still_running);
                printf("############ perform end %d ##############\n",still_running);
                break;
        }
    }while (still_running > 0);
    
    
    
    return 0;
}
#endif
