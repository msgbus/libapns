//
//  test_connection.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include <stdlib.h>
#include "YBAPNs.h"

#if 0
int main()
{
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object = YB_request_object_init(header, body, "pem", "aa/bb/cc", "123");
    YBAPNsConnection *connection = YB_connection_init(object);
    
    
    YBAPNsRequestHeader *header1 = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    YBAPNsRequestBody *body1 = YB_request_body_init("alert", 1, "default", NULL);
    YBAPNsRequestObject *object1 = YB_request_object_init(header1, body1, "pem", "abc-def", "vv-xx-ss");
    
    printf("============> test YB_connection_setHeader\n");
    printf("|connection NULL|\n");
    YB_connection_setHeader(NULL,object1);
    printf("|header NULL|\n");
    YB_connection_setHeader(connection, NULL);
    printf("|header added|\n");
    YB_connection_setHeader(connection, object);
    printf("|header|\n");
    YB_connection_setHeader(connection, object1);
    
    printf("============> test YB_connection_getRequest\n");
    printf("|connection NULL|\n");
    YB_connection_getRequest(NULL);
    printf("|connection not NULL|\n");
    YB_connection_getRequest(connection);
    
}
#endif
