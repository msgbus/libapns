//
//  test_headerget.c
//  testNewAPNs
//
//  Created by 云巴pro on 16/8/29.
//  Copyright © 2016年 Yunba.io. All rights reserved.
//

#include <stdio.h>
#include "YBAPNs.h"

#if 0
int main()
{
    YBAPNsRequestHeader *header = YB_request_header_init(kYBAPNsRequestMethodTypePOST, "abc-def", "vv-xx-ss", 100, kYBReqPriorityHigh, "app-bundleId");
    
    printf("==========> test YB_request_get_str_Attribute\n");
    printf("====> test NULL\n");
    printf("deviceToken:%s\n",YB_request_get_str_Attribute(NULL, kYBAPNsRequestAttributeDeviceToken));
    printf("apnsId:%s\n",YB_request_get_str_Attribute(NULL, kYBAPNsRequestAttributeApnsId));
    printf("apnsExpiration:%s\n",YB_request_get_str_Attribute(NULL, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsTopic:%s\n",YB_request_get_str_Attribute(NULL, kYBAPNsRequestAttributeApnsTopic));
    
    printf("====> test key\n");
    printf("deviceToken:%s\n",YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeDeviceToken));
    printf("apnsId:%s\n",YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeApnsId));
    printf("apnsExpiration:%s\n",YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsTopic:%s\n",YB_request_get_str_Attribute(header, kYBAPNsRequestAttributeApnsTopic));
    printf("key > 3:%s\n",YB_request_get_str_Attribute(header, 10));
    printf("key < 0:%s\n",YB_request_get_str_Attribute(header, -10));
    
    printf("==========> test YB_request_get_long_attribute\n");
    printf("====> test NULL\n");
    printf("deviceToken:%ld\n",YB_request_get_long_attribute(NULL, kYBAPNsRequestAttributeDeviceToken));
    printf("apnsId:%ld\n",YB_request_get_long_attribute(NULL, kYBAPNsRequestAttributeApnsId));
    printf("apnsExpiration:%ld\n",YB_request_get_long_attribute(NULL, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsTopic:%ld\n",YB_request_get_long_attribute(NULL, kYBAPNsRequestAttributeApnsTopic));
    
    printf("====> test key\n");
    printf("deviceToken:%ld\n",YB_request_get_long_attribute(header, kYBAPNsRequestAttributeDeviceToken));
    printf("apnsId:%ld\n",YB_request_get_long_attribute(header, kYBAPNsRequestAttributeApnsId));
    printf("apnsExpiration:%ld\n",YB_request_get_long_attribute(header, kYBAPNsRequestAttributeApnsExpiration));
    printf("apnsTopic:%ld\n",YB_request_get_long_attribute(header, kYBAPNsRequestAttributeApnsTopic));
    printf("key > 3:%ld\n",YB_request_get_long_attribute(header, 10));
    printf("key < 0:%ld\n",YB_request_get_long_attribute(header, -10));
    
    
    printf("==========> test YB_request_getApnsPriority\n");
    printf("====> test NULL\n");
    printf("priority:%d\n:",YB_request_getApnsPriority(NULL));
    printf("====> test Not NULL\n");
    printf("priority:%d\n:",YB_request_getApnsPriority(header));
    
    printf("==========> test YB_request_getAPNsMethod\n");
    printf("====> test NULL\n");
    printf("method:%d\n",YB_request_getAPNsMethod(NULL));
    printf("====> test Not NULL\n");
    printf("method:%d\n",YB_request_getAPNsMethod(header));
    return 0;
}


#endif