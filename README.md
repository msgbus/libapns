#YBAPNs
######p.s.需要curl7.50.2+，curl要带nghttp/2 和openssl 1.02e+
---
##Overview
该推送模块可以以简单的方式进行基于HTTP/2的新APNs的推送，调用者不需要与libcurl交互，直接调用模块相应的api即可实现推送，目前经过简单的测试，可以实现正确的推送。
接下来需要进行更加全面的测试，同时需要重新设计debug输出的格式
##使用方法

- ** 简单的使用方式 **

（尚未编写编译文件，目前还只能自己搭建环境）
需要环境：

		  1 libcouchbase
		  2 rabbitmq-c
		  3 libcurl --with-nghttp2 --with-openssl （7.50.2以上）

1.配置文件
	
	配置 apns.config 到可执行程序目录,有示例文件
	

2.启动

	启动可执行程序
	

